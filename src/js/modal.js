/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD');

// Creates the namepsace for the object
CRD.Modal = (function() {
	"use strict";
	
	var storage = {
			main : 'crd.modal'
		},
		constants = {
			modal   : {
				Events : {
					OPEN  : 'crd.modal.open',
					CLOSE : 'crd.modal.close'
				}
			},
			confirm : {
				Measure : {
					AUTO : 'auto'
				},
				Events : {
					CONFIRM : 'crd.confirm.confirm',
					CANCEL  : 'crd.confirm.cancel'
				}
			}
		};
	
	/**
	 * Modal
	 * @class
	 * @param {Object} options - Options to override default modal options.
	 * @return {Modal}
	 */
	
	function Modal(options) {
		
		// Set options
		this.setOptions(options);
		
		// initializes the modal
		this.Modal();
		
	}
	
	// Sidebar prototype implements ClassUtils
	Modal.prototype = jQuery.extend({
		
		/**
		 * Default options
		 * @property {Object} defaults                        - Default options
		 * @property {Object} defaults.target                 - Taget where the modal will be injected
		 * @property {Object} defaults.content                - Default content for the modal
		 * @property {Object} defaults.width                  - Width of the modal content
		 * @property {Object} defaults.height                 - Height of the modal content
		 * @property {Object} defaults.closable               - Is the modal closable?
		 * @property {Object} defaults.closekey               - Default close key (escape by default)
		 * @property {Object} defaults.overflow               - Hide container overflow?
		 * @property {Object} defaults.constructors           - Modal constructors
		 * @property {Object} defaults.constructors.container - Container constructor
		 * @property {Object} defaults.constructors.opaque    - Opaque element constructor
		 * @property {Object} defaults.constructors.content   - Content container constructor
		 */
		
		defaults : {
			target       : document.body,
			content      : null,
			top          : null,
			left         : null,
			right        : null,
			bottom       : null,
			width        : 400,
			height       : 250, // to-do : implement automatic width
			closable     : true,
			closekey     : 27,
			overflow     : false,
			constructors : {
				container : '<div class="modal" />',
				opaque    : '<div class="opaque" />',
				content   : '<div class="content" />'
			}
		},
		
		/**
		 * Initializes the modal
		 * @constructor
		 * @memberOf Modal
		 * @public
		 */
		
		Modal : function() {
			
			// Construct the modal
			this.construct();
			
			// Inserts the modal
			this.attach(this.options.target);
			
			// Injects the content
			this.injectContent(this.options.content);
			
			// Forces the element resize
			this.element.layout('get')
				.resize();
			
		},
		
		/**
		 * Creates the modal elements
		 * @method construct
		 * @return {Object}
		 * @memberOf Modal
		 * @public
		 */
		
		construct : function() {
			
			// If the element hasnt been created
			if(typeof this.element === 'undefined') {
				
				// Variables
				var opaque; // Non-referenced elements
				
				// Creates the container element and initializes the CRD.Layout instance
				this.element = jQuery(this.options.constructors.container);
				this.element.layout({ position : 'fixed' });
				
				// Creates the opacity element and initializes the CRD.Layout instance
				opaque = jQuery(this.options.constructors.opaque);
				opaque.layout();
				
				// If modal can be closed
				if(this.options.closable) {
					
					// Sets the close event (click on the opaque)
					opaque.click(this.close.bind(this));
					
					// Binds to the keyboard events
					jQuery(document.body).keyup(function(event) {
						
						// If escape has been pressed
						if(event.keyCode === this.options.closekey) {
							
							// Closes the modal
							this.close();
							
						}
						
					}.bind(this));
					
				}
				
				// Creates the container element and initializes the CRD.Layout instance
				this.content = jQuery(this.options.constructors.content);
				this.content.layout({
					top    : this.options.top,
					left   : this.options.left,
					right  : this.options.right,
					bottom : this.options.bottom,
					width  : this.options.width,
					height : this.options.height
				});
				
				// Hides the container until its content is setted
				this.content.hide();
				
				// Element nesting
				this.element.append(opaque, this.content);
				
				// Stores the reference to the element
				this.element.data(storage.main, this);
				
			}
			
			// Return the container element
			return this.element;
			
		},
		
		/**
		 * Injects the specified content in to the modal content container
		 * @method injectContent
		 * @param {Object|String} content - Injects the content using an HTML string or an object
		 * @return {Modal}
		 * @memberOf Modal
		 * @public
		 */
		
		injectContent : function(content) {
			
			// If the specified content is an objec
			if(jQuery(content).length > 0) {
				
				// Injects the element to the modal content container
				this.content.append(content);
				
			}
			
			// Otherwise the specified content is a string
			else {
				
				// Sets the specified content as the html of the element
				this.content.html(content);
				
			}
			
			// Shows the content
			this.content.show();
			
		},
		
		/**
		 * Append the modal to the specified element or document body
		 * @method attach
		 * @param {Object|String} where - Target where the modal will be appended
		 * @fires CRD.Modal.Events.CLOSE
		 * @return {Modal}
		 * @memberOf Modal
		 * @public
		 */
		
		attach : function(where) {
			
			// Sets the target
			where = jQuery(where || document.body);
			
			// If no element overflow is enabled
			if(this.options.overflow === false) {
				
				// Removes the element overflow
				where.css('overflow', 'hidden');
				
			}
			
			// Insets the modal to the specified element
			where.append(this.element);
			
			// Sets the state of the modal
			this.attached = true;
			
			// Attach the events to bind the resize events
			this.element.layout('get')
				.attachEvents();
			
			// Dispatch the corresponding event
			this.dispatch(constants.modal.Events.OPEN, [this.element, this]);
			
			// (:
			return this;
			
		},
		
		/**
		 * Removes the modal element for the DOM and returns it
		 * @method detach
		 * @fires CRD.Modal.Events.CLOSE
		 * @return {Object}
		 * @memberOf Modal
		 * @public
		 */
		
		detach : function() {
			
			// Variables
			var where = this.element.parent();
			
			// Sets the state of the modal
			this.attached = false;
			
			// If no element overflow is enabled
			if(this.options.overflow === false) {
				
				// Removes the element overflow
				where.css('overflow', 'auto');
				
			}
			
			// Dettach the events to bind the resize events
			this.element.layout('get')
				.detachEvents();
			
			// Dispatch the corresponding event
			this.dispatch(constants.modal.Events.CLOSE, [this.element, this]);
			
			// Returns the detached element
			return this.element.detach();
			
		},
		
		/**
		 * Closes the modal
		 * @method close
		 * @return {Object}
		 * @memberOf Modal
		 * @public
		 */
		
		close : function() {
			
			// If the modal is attached to the DOM
			if(this.attached === true) {
				
				// Returns the detached element
				return this.detach();
				
			}
			
		}
		
	}, CRD.ClassUtils);
	
	// Sets the constants
	CRD.Utils.setConstants(Modal, constants.modal);
	
	// Sets the helper to get the modal from an element
	jQuery.fn.getModal = function() {
		
		// Returns the first matched element modal instance
		return jQuery(this[0]).data(storage.main);
		
	};
	
	/**
	 * Confirm
	 * @class
	 * @param {Function} confirm - Function to execute when the user confirms the dialog
	 * @param {Function} cancel  - Function to execute when the user cancels the dialog
	 * @param {Object}   options - Options to override default modal options.
	 * @return {Confirm}
	 */
	
	function Confirm(confirm, cancel, options) {
		
		// Set the options
		this.setOptions(options);
		
		/**
		 * Function to execute when de dialog is confirmed
		 * @property {Function} confirmCallback - Confrmation callback function
		 */
		
		this.confirmCallback = confirm || function() {};
		
		/**
		 * Function to execute when de dialog is cancelled
		 * @property {Function} cancelCallback - Cancel callback function
		 */
		
		this.cancelCallback = cancel || function() {};
		
		// Apply base constructor with only rules as an argument)
		Modal.apply(this);
		
	}
	
	// Inherits from Modal
	Confirm.inherit(Modal);
	
	// Extends the base prototype
	Confirm.prototype = jQuery.extend(true, Confirm.prototype, {
		
		/**
		 * Default options
		 * @property {Object}  defaults                        - Default options
		 * @property {Object}  defaults.width                  - @override
		 * @property {Object}  defaults.height                 - @override or auto por automatic measurement
		 * @property {Object}  defaults.style                  - Modal style (sets confirm header color)
		 * @property {Object}  defaults.title                  - Confirm modal title
		 * @property {Object}  defaults.message                - Confrm message
		 * @property {Boolean} defaults.confirm                - Use confirm button?
		 * @property {Object}  defaults.confirmText            - Confirm button text
		 * @property {Boolean} defaults.cancel                 - Use cancel button?
		 * @property {Object}  defaults.cancelText             - Cancel button text
		 * @property {Object}  defaults.constructors           - Constructors
		 * @property {Object}  defaults.constructors.container - @override
		 * @property {Object}  defaults.constructors.confirm   - Confirmation dialog constructor
		 * @see Modal.prototype.defaults
		 */
		
		defaults : {
			width        : 300,
			height       : constants.confirm.Measure.AUTO,
			style        : 'error',
			title        : 'Atención!',
			message      : '¿Está seguro?',
			confirm      : true,
			confirmText  : 'Aceptar',
			cancel       : true,
			cancelText   : 'Cancel',
			constructors : {
				container : '<div class="modal confirm" />',
				confirm   : '<div>' +
						'<div class="title"></div>' +
						'<div class="message"></div>' +
						'<div class="buttons"></div>' +
					'</div>'
			}
		},
		
		/**
		 * Injects the specified content in to the modal content container
		 * @override Modal.injectContent
		 * @return {Confirm}
		 * @memberOf Confirm
		 * @public
		 */
		
		injectContent : function() {
			
			// Variables
			var self = this,
				content = jQuery(this.options.constructors.confirm),
				confirm = this.options.confirm !== false ? new CRD.UI.Button({ style : 'success' }, this.options.confirmText, this.confirm.bind(this)) : null,
				cancel = this.options.cancel !== false ? new CRD.UI.Button({ style : 'error' }, this.options.cancelText, this.cancel.bind(this)) : null,
				append = [],
				buttons = content.find('.buttons'),
				size;
			
			// If a confirm button exists
			if(confirm !== null) {
				
				// Append the confirm button
				append.push(confirm.element);
				
			}
			
			// If a cancel button exists
			if(cancel !== null) {
				
				// Append the cancel button
				append.push(cancel.element);
				
			}
			
			// Sets the style of the modal
			this.content.addClass(this.options.style);
			
			// If there are any buttons to append
			if(append.length > 0) {
				
				// Inserts the buttons
				buttons.append(append);
				
			} else {
				
				// Removes the button holder
				buttons.remove();
				
			}
			
			// Sets the texts
			content.find('.title').text(this.options.title);
			content.find('.message').text(this.options.message);
			
			// Adds the dialog to the modal
			this.content.append(content);
			
			// If height has to be setted automatically
			if(this.options.height === 'auto'){
				
				// Measures the modal content
				size = this.element.measure(function() {
					var element = jQuery(this),
						value;
					element.find('.content')
						.css('width', self.options.width)
						.show();
					value = {
						width  : element.width(),
						height : element.height()
					};
					element.remove();
					return value;
				});
				
				// Sets the content size
				this.content.layout('setOptions', {height : size.height});
				
			}
			
			// Shows the content (delayed, so the content will be fully rendered when displayed)
			this.content.show.delay(50, this.content);
			
			// (:
			return this;
			
		},
		
		/**
		 * Confirms the dialog prompt
		 * @method confirm
		 * @memberOf Confirm
		 * @public
		 */
		
		confirm : function() {
			
			// Dispatch the event
			this.dispatch(constants.confirm.Events.CONFIRM);
			
			// Executes the callback
			this.confirmCallback.apply(this);
			
			// Closes the modal
			this.close();
			
		},
		
		/**
		 * Cancel the confirmation dialog
		 * @method cancel
		 * @memberOf Confirm
		 * @public
		 */
		
		cancel : function() {
			
			// Dispatch the event
			this.dispatch(constants.confirm.Events.CANCEL);
			
			// Executes the callback
			this.cancelCallback.apply(this);
			
			// Closes the modal
			this.close();
			
		}
		
	});
	
	// Sets the constants
	CRD.Utils.setConstants(Confirm, constants.confirm);
	
	// Adds Confirm to the Modal namespace
	Modal.Confirm = Confirm;
	
	// Returns the modal
	return Modal;
	
})();