/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD');

// Creates the namepsace for the object
(function(context) {
	"use strict";
	
	// Variables
	var constants = {
			clonable          : {
				Events    : {
					REGISTER     : 'crd.clonable.register',
					CLONE        : 'crd.clonable.clone',
					CLONE_INJECT : 'crd.clonable.cloneinject',
					MAX_REACHED  : 'crd.clonable.maxreached',
					REMOVE       : 'crd.clonable.remove'
				}
			},
			multipleselection : {
				Modes     : {
					INLINE  : 'inline',
					DEFAULT : 'default'
				},
				Events    : {
					REGISTER     : 'crd.multipleselection.register',
					ADD          : 'crd.multipleselection.add',
					MAX_REACHED  : 'crd.multipleselection.maxreached',
					REMOVE       : 'crd.multipleselection.remove'
				}
			}
		}, // Constants
		storage = {
			clonable          : {
				main  : 'crd.clonable'
			},
			multipleselection : {
				main     : 'crd.multipleselection',
				value    : 'crd.multipleselection.value',
				disabled : 'crd.multipleselection.disabled',
				html     : {
					value    : 'multipleselection-value',
					disabled : 'multipleselection-disabled'
				}
			}
		}, // Data and HTML storage
		extend = jQuery.extend; // jQuery extend shortcut
	
	/**
	 * Clonable
	 * Clone areas
	 * @class
	 * @param {String|Object} container - Target element (cloned elements container)
	 * @param {String|Object} source    - Cource element (cloned element)
	 * @param {Object}        options   - Options to override default sidebar options.
	 * @return {Clonable}
	 */
	
	function Clonable(container, source, options) {
		
		// Element reference
		container = jQuery(container);
		
		// Variables
		var self = container.data(storage.clonable.main); // Data storage reference
		
		// If instance hasn't been created yet
		if(typeof self === 'undefined') {
			
			// Set options and events
			this.setOptions(options);
			
			/**
			 * Container for cloned elements
			 * @property {Object} container - Element
			 */
			
			this.container = container;
			
			/**
			 * Clone source
			 * @property {Object} source - Element
			 */
			
			this.source = jQuery(source);
			
			/**
			 * Cloned elements count
			 * @property {Number} count - Element
			 */
			
			this.count = 0;
			
			/**
			 * Cloned elements collection
			 * @property {Object} clones - Elements collection
			 */
			
			this.clones = [];
			
			// Initializes the object
			this.Clonable();
			
			// Stores the object reference in the element storage
			this.container.data(storage.clonable.main, this);
			
			// Sets data reference
			self = this;
			
		}
		
		// Returns object reference
		return self;
		
	}
	
	// Clonable prototype implements ClassUtils
	Clonable.prototype = extend({
		
		/**
		 * Default options
		 * @property {Object}  defaults         - Default options
		 */
		
		defaults : {
			button       : true,
			insert       : 'append',
			max          : false, // No maximum
			remove       : '.remove',
			register     : true,
			constructors : {
				button : '<a href="javascript:;" class="button">{text}</a>'
			},
			language     : {
				button : 'Agregar'
			}
		},
		
		/**
		 * Initializes the clonable
		 * @constructor
		 * @memberof Clonable
		 * @public
		 */
		
		Clonable : function() {
			
			// Assign the button
			this.button = (this.options.button === true) ?
				this.buildButton() :
				jQuery(this.options.button);
			
			// Adds the button event
			this.button.click(this.clone.bind(this));
			
			// Register the already cloned elements
			this.registerAvailable();
			
		},
		
		/**
		 * Creates the clomning button
		 * @method buildButton
		 * @memberof Clonable
		 * @return {Object}
		 * @public
		 */
		
		buildButton : function() {
			
			// Variables
			var button = jQuery(this.options.constructors.button.substitute({ text : this.options.language.button }));
			
			// Returns the built button
			return button;
			
		},
		
		/**
		 * Register already available elements
		 * @method registerAvailable
		 * @memberof Clonable
		 * @fires crd.clonable.register
		 * @public
		 */
		
		registerAvailable : function() {
			
			// Variables
			var self = this;
			
			// For each available item
			this.container.children().each(function(i, e) {
				
				// Normalizes element
				e = jQuery(e);
				
				// Increases the cloning count
				self.count++;
				
				// Adds the clones element to the cloned elements collection
				self.clones.push(e);
				
				// Adds the remove action
				e.find(self.options.remove).click(self.remove.bind(self, e));
				
				/**
				 * Triggers the clone event on the element
				 * @event crd.clonable.clone
				 */
				
				self.dispatch(constants.clonable.Events.REGISTER, [
					e,
					self.count,
					self.container,
					self
				], self);
				
			});
			
		},
		
		/**
		 * Clone and elements and adds the result to the container
		 * @method buildButton
		 * @memberof Clonable
		 * @fires crd.clonable.clone
		 * @fires crd.clonable.cloneinject
		 * @fires crd.clonable.maxreached
		 * @return {Object}
		 * @public
		 */
		
		clone : function() {
			
			// Variables
			var cloned;
			
			// If the maximum elements are defined and that maximum has not been reeached
			if(this.options.max === false || this.count < this.options.max) {
				
				// Increases the cloning count
				this.count++;
				
				// Clone the element
				cloned = this.source.clone();
				
				/**
				 * Triggers the clone event on the element
				 * @event crd.clonable.clone
				 */
				
				this.dispatch(constants.clonable.Events.CLONE, [
					cloned,
					this.count,
					this.container,
					this
				], this);
				
				// Adds the remove action
				cloned.find(this.options.remove).click(this.remove.bind(this, cloned));
				
				// Adds the cloned element to the container
				this.container[this.options.insert](cloned);
				
				// Adds the cloned element to the cloned collection
				this.clones.push(cloned);
				
				/**
				 * Triggers the clone injection event on the element
				 * @event crd.clonable.cloneinject
				 */
				
				this.dispatch(constants.clonable.Events.CLONE_INJECT, [
					cloned,
					this.count,
					this.container,
					this
				], this);
				
			}
			
			if(this.count === this.options.max) {
				
				/**
				 * Triggers the clone maximum reached event on the element
				 * @event crd.clonable.maxreached
				 */
				
				this.dispatch(constants.clonable.Events.MAX_REACHED, [
					cloned ? cloned : null,
					this.count,
					this.container,
					this
				], this);
				
			}
			
		},
		
		/**
		 * Removes a cloned element
		 * @method remove
		 * @memberof Clonable
		 * @param {Object} remove - Element to remove
		 * @fires crd.clonable.remove
		 * @return {Object}
		 * @public
		 */
		
		remove : function(remove) {
			
			// Minus one
			this.count--;
			
			// Removed the element from the clones array
			this.clones.splice(this.clones.indexOf(remove), 1);
			
			/**
			 * Triggers the remove event
			 * @event crd.clonable.remove
			 */
			
			this.dispatch(constants.clonable.Events.REMOVE, [
				remove,
				this.count,
				this.container,
				this
			], this);
			
			// Removes and return the removed element
			return remove.detach();
		}
		
	}, CRD.ClassUtils);
	
	// Set object constants
	CRD.Utils.setConstants(Clonable, constants.clonable);
	
	// Defines the jQuery helper
	CRD.Utils.defineHelper(Clonable, 'clonable', storage.clonable.main);
	
	/**
	 * MultipleSelection
	 * Multiple selection control
	 * @class
	 * @param {String|Object} element - Source element (multiple selectior combo)
	 * @param {Object}        options - Options to override default multiple selector options.
	 * @return {MultipleSelection}
	 */
	
	function MultipleSelection(element, options) {
		
		// Element reference
		element = jQuery(element);
		
		// Variables
		var self = element.data(storage.multipleselection.main); // Data storage reference
		
		// If instance hasn't been created yet
		if(typeof self === 'undefined') {
			
			// Set options and events
			this.setOptions(options);
			
			/**
			 * Source element
			 * @property {Object} element - Element
			 */
			
			this.element = element;
			
			// Initializes the object
			this.MultipleSelection();
			
			// Stores the object reference in the element storage
			this.element.data(storage.multipleselection.main, this);
			
			// Sets data reference
			self = this;
			
		}
		
		// Returns object reference
		return self;
		
	}
	
	// MultipleSelection prototype implements ClassUtils
	MultipleSelection.prototype = extend({
		
		/**
		 * Default options
		 * @property {Object}  defaults         - Default options
		 */
		
		defaults : {
			mode         : constants.multipleselection.Modes.DEFAULT,
			insert       : 'append',
			max          : false, // No maximum
			register     : true,
			ignoreempty  : true,
			firstoption  : 'Seleccione una opción...',
			classes      : {
				inline  : {
					initial  : 'button',
					default  : 'default',
					disabled : 'disabled',
					selected : 'success'
				},
				default : {
					initial : 'button',
					default : 'success'
				}
			},
			constructors : {
				inline : {
					container : '<div class="multiple-selection inline" />',
					option    : '<div>{text}</div>'
				},
				default : {
					container : '<div class="multiple-selection default" />',
					select    : '<select />',
					option    : '<div><i class="fa fa-close"></i><span>{text}</span></div>'
				}
			}
		},
		
		/**
		 * Initializes the multiple selector
		 * @constructor
		 * @memberof MultipleSelection
		 * @public
		 */
		
		MultipleSelection : function() {
			
			// Prepares the original input, builds the UI elements and set selected items
			this.prepareInput()
				.createControls()
				.updateSelection();
			
		},
		
		/**
		 * Prepares the original input field
		 * @return {MultipleSelection}
		 */
		
		prepareInput : function() {
			
			// Hides the element and make sure to set it as multiple selection
			this.element.hide().attr('multiple', true).find('option[value=""]').remove();
			
			// Hooks to the original element change
			this.element.on('change select', this.updateSelection.bind(this));
			
			// (:
			return this;
			
		},
		
		/**
		 * Creates the UI controls
		 * @method createControls
		 * @memberof MultipleSelection
		 * @return {Object:MultipleSelection}
		 * @public
		 */
		
		createControls : function() {
			
			// Variales:
			var self = this,
				parent = this.element.parent(),
				options = this.element.find('option'),
				generated = [],
				option;
			
			// Creates the option container
			this.container = jQuery(this.options.constructors[this.options.mode].container);
			
			// Depending on the mode
			switch(this.options.mode) {
				
				// Inline (all options are displayed as on/off buttons)
				case constants.multipleselection.Modes.INLINE:
					
					// For each option
					options.each(function(i, e) {
						
						// Normalizes the option
						e = jQuery(e);
						
						// Variables
						var value    = e.val(),
							disabled = typeof e.attr('disabled') !== 'undefined' || false;
						
						// If the option has a value
						if((self.options.ignoreempty === true && value !== '') || self.options.ignoreempty === false) {
						
							// Creates the option
							option = self.buildOption(e.text(), value, disabled);
							
							// Adds the element
							generated.push(option);
						
						}
						
					});
					
					// Add all the options and adds the container
					parent.append(this.container.append(generated));
					
					break;
					
				// Default
				default:
					
					// Creates the input clone
					this.clone = jQuery(this.options.constructors.default.select);
					
					// Clones the element
					this.clone.html(this.element.html());
					
					// If empty values must be ignored
					if(this.options.ignoreempty === true) {
						
						// Remove all empty values options
						this.clone.find('option[value=""]').remove();
						
					}
					
					// Creates the first option
					this.clone.prepend(jQuery('<option />').attr('value', '').text(this.options.firstoption));
					
					// Selects the first option
					this.clone.val('');
					
					// Hooks to the clone element change
					this.clone.on('change select', function() {
						
						// Variables
						var clone = jQuery(this),
							value = clone.val();
						
						// Toggle the element selection
						self.toggleOption(clone.find('[value="' + value + '"]'));
						
						// Resets te value selection
						clone.val('');
						
						// ):
						return false;
						
					});
					
					// Adds the elements
					parent.append(this.clone, this.container);
					
					break;
				
			}
			
			// Hooks the event to all the added buttons
			this.container.on('click', '.button', function(event) {
				
				// Variables
				var target = jQuery(event.target);
				
				// Toggle the element selection
				self.toggleOption(target.is('.button') ? target : target.closest('.button'));
				
			});
			
			// (:
			return this;
			
		},
		
		/**
		 * Update selected elements in original element
		 * @method updateSelection
		 * @memberof MultipleSelection
		 * @fires crd.multipleselection.maxreached
		 * @param {Object} event - change event
		 * @return {Object:MultipleSelection}
		 * @public
		 */
		
		updateSelection : function(event) {
			
			// Variables
			var selected = this.element.val() || [],
				classes = this.options.classes[this.options.mode],
				related, option;
			
			// Depending on the mode
			switch(this.options.mode) {
				
				// Inline (all options are displayed as on/off buttons)
				case constants.multipleselection.Modes.INLINE:
					
					// For each option
					this.container.find('[data-' + storage.multipleselection.html.value + ']').each(function(i, e) {
						
						// Variables
						var option = jQuery(e),
							value = option.data(storage.multipleselection.value),
							add, remove;
						
						// If the elements is selected
						if(selected.indexOf(value) >= 0) {
							
							// Classes to add/remove
							add    = classes.selected;
							remove = classes.default;
							
						} else {
							
							// Classes to add/remove
							add    = classes.default;
							remove = classes.selected;
							
						}
						
						// Toggle the classes
						option.removeClass(remove)
							.addClass(add);
						
					});
					
					break;
				
				// Default
				default:
					
					// Remove selection
					this.container.empty();
					
					// For each selected option
					for(var x = 0, max = selected.length; x < max; x = x + 1) {
						
						// gets the related option
						related = this.element.find('option[value="' + selected[x] + '"]');
						
						// Creates the option
						option = this.buildOption(related.text(), selected[x]);
						
						// Adds the option to the container
						this.container.append(option);
						
					}
					
					break;
				
			}
			
			// Update disabled options
			this.updateDisabled();
			
			// If the maximum selection has been reached
			if(selected.length === this.options.max) {
				
				/**
				 * Triggers the maximum selection reached event on the element
				 * @event crd.clonable.maxreached
				 */
				
				this.dispatch(constants.clonable.Events.MAX_REACHED, [
					this.element,
					this
				], this);
				
			}
			
			// (:
			return this;
			
		},
		
		/**
		 * Update disabled elements
		 * @method updateDisabled
		 * @memberof MultipleSelection
		 * @return {Object:MultipleSelection}
		 * @public
		 */
		
		updateDisabled : function() {
			
			// Variables
			var self, selected, classes;
			
			// If maximum item selection is enabled or is in default mode
			if(this.options.max !== false || this.options.mode === constants.multipleselection.Modes.DEFAULT) {
				
				// Set some variables
				self     = this;
				selected = this.element.val() || [];
				classes  = this.options.classes[this.options.mode];
				
				// Depending on the mode
				switch(this.options.mode) {
					
					// Inline (all options are displayed as on/off buttons)
					case constants.multipleselection.Modes.INLINE:
						
						// For each non-selected option
						this.container.find('.' + classes.default).each(function(i, e) {
							
							// Normalizes element
							e = jQuery(e);
							
							// If the option is not disabled
							if(e.data(storage.multipleselection.disabled) === false) {
								
								// Disables/enables the option
								e[selected.length === self.options.max ? 'addClass' : 'removeClass'](classes.disabled);
								
								// Enables / disables the combo options
								self.element.find('[value="' + e.data(storage.multipleselection.value) + '"]')
									.attr('disabled', selected.length === self.options.max);
								
							}
							
						});
						
						break;
						
					default:
						
						// For each option
						this.element.find('option').each(function(i, e) {
							
							// Normalizes the element
							e = jQuery(e);
							
							// Variales
							var value = e.val(),
								isselected = selected.indexOf(value) >= 0,
								related = self.clone.find('option[value="' + value + '"]'),
								disabled = typeof e.attr('disabled') !== 'undefined' || false;
							
							// If the options is disabled or the maximum selection is enabled
							if(disabled === true || (self.options.max !== false && selected.length === self.options.max && isselected === false)) {
								
								// Disables/enables the option
								related.attr('disabled', true);
							
							} else {
								
								// Disables/enables the option
								related.attr('disabled', isselected);
								
							}
							
						});
						
						break;
					
				}
				
			}
			
			// (:
			return this;
			
		},
		
		/**
		 * Toggle selection of an inline element
		 * @method toggleOption
		 * @memberof MultipleSelection
		 * @fires crd.multipleselection.add
		 * @fires crd.multipleselection.remove
		 * @fires crd.multipleselection.maxreached
		 * @param {Object} target - Clicked option
		 * @return {Object:MultipleSelection}
		 * @public
		 */
		
		toggleOption : function(target) {
			
			// Variables
			var clicked = jQuery(target),
				value = clicked.data(storage.multipleselection.value) || clicked.attr('value'),
				selected = this.element.val() || [],
				index = selected.indexOf(value),
				classes = this.options.classes[this.options.mode],
				disabled = clicked.data(storage.multipleselection.disabled),
				option;
			
			// Normalizes the disabled value
			disabled = typeof disabled !== 'undefined' && disabled === true;
			
			// If the maximum elements are defined and that maximum has not been reeached
			if((this.options.max === false || selected.length < this.options.max || (selected.length === this.options.max && index >= 0)) && disabled === false) {
				
				// If the item is not selected
				if(index < 0) {
					
					// Push the value to the selected array
					selected.push(value);
					
					// Set the selected options
					this.element.val(selected);
					
					// Depending on the mode
					switch(this.options.mode) {
						
						// Inline (all options are displayed as on/off buttons)
						case constants.multipleselection.Modes.INLINE:
							
							// Toggle the classes
							clicked.removeClass(classes.default)
								.addClass(classes.selected);
							
							break;
						
						default:
							
							// Creates the option
							option = this.buildOption(clicked.text(), value);
							
							// Adds the option to the container
							this.container.append(option);
							
							break;
						
					}
					
					/**
					 * Triggers the add event
					 * @event crd.multipleselection.remove
					 */
					
					this.dispatch(constants.multipleselection.Events.ADD, [
						value,
						this.element,
						this
					], this);
					
				}
				else {
					
					// Remove the selected element from the selected array
					selected.splice(index, 1);
					
					// Set the selected options
					this.element.val(selected);
					
					// Depending on the mode
					switch(this.options.mode) {
						
						// Inline (all options are displayed as on/off buttons)
						case constants.multipleselection.Modes.INLINE:
							
							// Toggle the classes
							clicked.removeClass(classes.selected)
								.addClass(classes.default);
							
							break;
						
						default:
							
							// Removes the clicked element
							clicked.remove();
							
							break;
						
					}
					
					/**
					 * Triggers the remove event
					 * @event crd.multipleselection.remove
					 */
					
					this.dispatch(constants.multipleselection.Events.REMOVE, [
						value,
						this.element,
						this
					], this);
					
				}
				
			}
			
			// Update disabled elements
			this.updateDisabled();
			
			// If the maximum selection has been reached
			if(selected.length === this.options.max) {
				
				/**
				 * Triggers the maximum selection reached event on the element
				 * @event crd.clonable.maxreached
				 */
				
				this.dispatch(constants.clonable.Events.MAX_REACHED, [
					this.element,
					this
				], this);
				
			}
			
			// (:
			return this;
			
		},
		
		/**
		 * Creates and option button/tag
		 * @method buildOption
		 * @memberof MultipleSelection
		 * @param {String}  label    - Option label
		 * @param {String}  value    - Option value
		 * @param {Boolean} disabled - Option disabled status
		 * @return {Object}
		 * @public
		 */
		
		buildOption : function(label, value, disabled) {
			
			// Variables
			var option = jQuery(this.options.constructors[this.options.mode].option.substitute({ text : label })),
				classes = this.options.classes[this.options.mode];
			
			// Normalizes the disabled status
			disabled = typeof disabled !== 'undefined' && disabled === true;
			
			// Adds the initial classes
			option.addClass([
				classes.initial,
				classes.default,
				disabled === true ? classes.disabled : null
			].join(' '));
			
			// Set the data and attributes
			option
				.attr('data-' + storage.multipleselection.html.value, value)
				.data(storage.multipleselection.value, value)
				.attr('data-' + storage.multipleselection.html.disabled, disabled)
				.data(storage.multipleselection.disabled, disabled);
			
			// Returns the built option
			return option;
			
		}
		
	}, CRD.ClassUtils);
	
	// Set object constants
	CRD.Utils.setConstants(MultipleSelection, constants.multipleselection);
	
	// Defines the jQuery helper
	CRD.Utils.defineHelper(MultipleSelection, 'multipleSelection', storage.multipleselection.main);
	
	// Returns the form related classes
	context = extend(true, context, {
		Forms : {
			'Clonable'          : Clonable,
			'MultipleSelection' : MultipleSelection
		}
	});
	
})(CRD);