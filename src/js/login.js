/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD.UI');

// Creates the namepsace for the object
(function(context) {
	"use strict";
	
	// Variables
	var config = context.Config, // Configuration
		extend = jQuery.extend; // jQuery.extend shortcut
	
	/**
	 * Login
	 * @class
	 * @param {Object} element - Form element.
	 * @param {Object} options - Options to override default options.
	 * @return {Login}
	 */
	
	function Login(element, options) {
		
		// Variables
		var self = CRD.AjaxForm.apply(this, [element, options]);
		
		// Set layout
		this.Login();
		
	}
	
	// Inherits from CRD.AjaxForm
	Login.inherit(CRD.AjaxForm);
	
	// Function prototype
	Login.prototype = extend(true, Login.prototype, {
		
		defaults : {
			wrapper : '#wrapper',
			loading : '.loading',
			output  : '#message > div'
		},
		
		/**
		 * Set layout elements
		 * @constructor
		 * @memberOf Login
		 * @listen this.element.submit
		 * @public
		 */
		
		Login : function() {
			
			// Variables
			var options = {
				disabledBefore : 0
			}; // Common options
			
			// Main wrapper container
			jQuery('#wrapper').layout(options);
			
			// Login error/success background hack
			jQuery('#error-advice').layout(options);
			jQuery('#success-advice').layout(options);
			
			// Login box
			jQuery('#login')
				.layout(extend(true, {
					width  : config.Login.formWidth,
					height : 420,
					top    : null,
					right  : null,
					bottom : null,
					left   : null
				}, options));
			
			// Footer copy
			jQuery('#copy')
				.layout(extend(true, {
					height : 20,
					top    : null,
					right  : 0,
					bottom : 0,
					left   : 0
				}, options));
			
			// Set reference to loading wrapper, animation and error container
			this.wrapper = jQuery(this.options.wrapper);
			this.loading = jQuery(this.options.loading);
			this.output = jQuery(this.options.output);
			
			// Hook to AjaxForm events
			jQuery(this).on(CRD.AjaxForm.Events.SUCCESS, this.dataLoaded.bind(this));
			jQuery(this).on(CRD.AjaxForm.Events.ERROR, this.dataError.bind(this));
			
			// Hooks to form submit
			this.element.submit(this.doLogin.bind(this));
			
		},
		
		/**
		 * Perform the login sequence
		 * @method doLogin
		 * @param {Object} event - Submit event
		 * @memberOf Login
		 * @public
		 */
		
		doLogin : function(event) {
			
			// Prevent default
			event.preventDefault();
			
			// Removes error messages
			this.wrapper.removeClass('error');
			
			// Hides output message
			this.output.parent().hide();
			
			// Shows loading animation
			this.loading.show();
			
			// Send form
			this.send.delay(1000, this);
			
		},
		
		/**
		 * Handles the successfull data loading from server
		 * @method dataLoaded
		 * @param {Object} event  - CRD.AjaxForm event
		 * @param {Object} data   - Loaded JSON data
		 * @param {Number} status - Request status code
		 * @param {Object} xhr    - Request XHR object
		 * @memberOf Login
		 * @public
		 */
		
		dataLoaded : function(event, data, status, xhr) {
			
			// If login has been successful
			if(data.hasOwnProperty('error') === false || data.error === false) {
				
				// Success animation (thru CSS)
				this.wrapper
					.removeClass('error')
					.addClass('success');
				
				// Redirects to supplied url
				(function() {
					location.href = data.redirect;
				}).delay(100);
				
			}
			
			// Login error
			else {
				
				// Error animation (thru CSS)
				this.wrapper
					.removeClass('success')
					.addClass('error');
				
				// Hides login animation
				this.loading.hide();
				
			}
			
			// Sets the output message
			this.output
				.text(data.message)
				.parent().show();
			
		},
		
		/**
		 * Handles data communication error
		 * @method dataError
		 * @param {Object} event  - CRD.AjaxForm event
		 * @param {Object} xhr    - Request XHR object
		 * @param {Number} status - Request status code
		 * @param {Object} error  - Error message
		 * @memberOf Login
		 * @public
		 */
		
		dataError : function(event, xhr, status, error) {
			
			// Error animation (thru CSS)
			this.wrapper
				.removeClass('success')
				.addClass('error');
			
			// Hides login animation
			this.loading.hide();
			
			// Sets the output message
			this.output
				.text('Ha ocurrido un error al intentar conectarse al servidor (Error: ' + xhr.status + ')') // to-do : implement internationalization
				.parent().show();
			
		}
		
	});
	
	// Returns the interface classes
	context = extend(true, context, {
		'Login' : Login
	});
	
})(CRD.UI);