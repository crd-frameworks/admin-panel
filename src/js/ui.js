/* globals namespace, CRD */
/* jshint -W058 */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD');

// Creates the namepsace for the object
(function(context) {
	"use strict";
	
	// Variables
	var constants = {
			sidebar : {
				Mode      : {
					OPEN  : 'open',
					CLOSE : 'close'
				},
				Events    : {
					OPEN   : 'crd.sidebar.open',
					CLOSE  : 'crd.sidebar.close',
					TOGGLE : 'crd.sidebar.toggle'
				}
			},
			button : {
				Events : {
					CLICK   : 'crd.button.click',
					ENABLE  : 'crd.button.enable',
					DISABLE : 'crd.button.dissable'
				}
			},
			collapsable : {
				Mode      : {
					OPEN  : 'open',
					CLOSE : 'close'
				},
				Events    : {
					OPEN   : 'crd.collapsable.open',
					CLOSE  : 'crd.collapsable.close',
					TOGGLE : 'crd.collapsable.toggle'
				}
			}
		}, // Constants
		storage = {
			sidebar : {
				html    : {
					state : 'sidebar-state'
				},
				session : {
					state : 'crd.sidebar.state'
				}
			},
			layout  : {
				html : 'layout-options'
			},
			toolbar : 'crd.toolbar',
			button  : 'crd.button',
			collapsable : {
				main  : 'crd.collapsable',
				state : 'crd.collapsable.state',
				html  : {
					main  : 'collapsable',
					state : 'collapsed'
				}
			}
		}, // Data and HTML storage
		session = CRD.Storage.Session, // Session storage
		config = context.UI.Config, // Configuration
		extend = jQuery.extend; // jQuery extend shortcut
	
	/**
	 * Sidebar
	 * Implements CRD.Layout to build a layout with a container and a left sidebar.
	 * to-do : Implement left/right sidebar
	 * @class
	 * @param {Object} options - Options to override default sidebar options.
	 * @return {Sidebar}
	 */
	
	function Sidebar(options) {
		
		// If singleton has not been initialized
		if(Sidebar.prototype.instance === null) {
			
			// Set options and events
			this.setOptions(options);
			
			/**
			 * State of the sidebar
			 * @property {String|Boolean} state - Current state of the sidebar
			 */
			
			this.state = this.options.defaultState;
			
			/**
			 * Element to use as sidebar/container wrapper
			 * @property {Object} element - Wrapper
			 */
			
			this.wrapper = jQuery(this.options.wrapper);
			
			/**
			 * Element to use as sidebar
			 * @property {Object} element - Sidebar
			 */
			
			this.sidebar = jQuery(this.options.sidebar);
			
			/**
			 * Element to use as container
			 * @property {Object} element - Container
			 */
			
			this.container = jQuery(this.options.container);
			
			// Initializes the object
			this.Sidebar();
			
			// Sets the singleton instance
			Sidebar.prototype.instance = this;
			
		}
		
		// Returns the singleton instance
		return Sidebar.prototype.instance;
		
	}
	
	// Sidebar prototype implements ClassUtils
	Sidebar.prototype = extend({
		
		/**
		 * Singleton instance object
		 * @property {Object} instance - Singleton pattern instance
		 */
		
		instance : null,
		
		/**
		 * Default options
		 * @property {Object}         defaults                - Default options
		 * @property {String|Boolean} defaults.defaultState   - Sidebar default state
		 * @property {String}         defaults.wrapper        - Sidebar wrapper
		 * @property {String}         defaults.sidebar        - Sidebar selector
		 * @property {String}         defaults.container      - Content container selector
		 * @property {String}         defaults.toggler        - Toggler selector
		 * @property {Object}         defaults.width          - Sidebar widths
		 * @property {Number}         defaults.width.opened   - Sidebar opened width
		 * @property {Number}         defaults.width.closed   - Sidebar closed width
		 * @property {Number}         defaults.disabledBefore - Disable sidebar before (breakpoint)
		 * @property {Object}         defaults.extraOptions   - Extra options for the layout class, referenced by the
		 * 														key of the element
		 */
		
		defaults : {
			defaultState   : constants.sidebar.Mode.OPEN,
			wrapper        : '#wrapper',
			sidebar        : '#sidebar',
			container      : '#container',
			toggler        : '.toggle-sidebar',
			width          : {
				open  : config.Sidebar.openWidth,
				close : config.Sidebar.closedWidth
			},
			disabledBefore : config.Sidebar.disableBefore,
			extraOptions   : {}
		},
		
		/**
		 * Initializes the layout w/sidebar UI
		 * @constructor
		 * @memberof Sidebar
		 * @public
		 */
		
		Sidebar : function() {
			
			// Variables
			var state;
			
			// Get html tag defined state
			state = session.getData(storage.sidebar.session.state, undefined) || this.sidebar.data(storage.sidebar.html.state);
			
			// If state of the sidebar is defined as html tag data
			if(typeof state !== 'undefined') {
				
				// Set the initial state
				state = state === constants.sidebar.Mode.OPEN || Number(state) === 1 ?
					constants.sidebar.Mode.OPEN :
					constants.sidebar.Mode.CLOSE;
				
			}
			
			// Builds the sidebar
			this.setLayout(state || this.options.defaultState);
			
			// Creates the click handler for the sidebar toggler
			this.clickHandler = function(e) {
				this.toggleSidebar();
			}.bind(this);
			
			// Adds the toggle sidebar event
			jQuery(this.options.toggler).click(this.clickHandler);
			
		},
		
		/**
		 * Sets the layout elements
		 * @method setLayout
		 * @param {String|Boolean} method - State of the sidebar (open|close)
		 * @fires CRD.Sidebar.Events.OPEN
		 * @fires CRD.Sidebar.Events.CLOSE
		 * @fires CRD.Sidebar.Events.TOGGLE
		 * @return {Sidebar}
		 * @memberOf Sidebar
		 * @public
		 */
		
		setLayout : function(method) {
			
			// Method
			method = method || this.state;
			
			// Variables
			var elements = {
					'wrapper'   : {},
					'sidebar'   : { width : this.options.width[method] },
					'container' : { left  : this.options.width[method] }
				}, // Referenced options, to-do : is this the best way to do this?
				event = method === constants.sidebar.Mode.CLOSE ? constants.sidebar.Events.CLOSE : constants.sidebar.Events.OPEN, // Event to trigger
				element, options, extra, layout;
			
			// For each defined element
			for(var e in elements) {
				
				// Filters unwanted properties
				if(elements.hasOwnProperty(e)) {
					
					// Sets extra options for the element
					extra = this.options.extraOptions.hasOwnProperty(e) ?
						this.options.extraOptions[e] :
						{};
					
					// Sets elements options
					options = extend({
						disabledBefore : this.options.disabledBefore
					}, extra, elements[e]);
					
					// Set the elements by its inner object reference (object key match class property reference)
					element = jQuery(this[e]);
					
					// Set element layout
					layout = this.setElementLayout(element, options);
					
				}
				
			}
			
			// Triggers the resize for the first element
			this.wrapper.layout('resize', true);
			
			/**
			 * Triggers the open or close event
			 * @event crd.sidebar.open
			 * @event crd.sidebar.close
			 * @event crd.sidebar.toggle
			 */
			
			this.dispatch([
				{
					event : event,
					args  : [
						this.sidebar,
						this.container,
						this
					]
				},
				{
					event : constants.sidebar.Events.TOGGLE,
					args  : [
						method,
						this.sidebar,
						this.container,
						this
					]
				}
			]);
			
			// If the sidebar is not disabled
			if(this.disabled() === false) {
				
				// Stores the sate in session
				session.setData(storage.sidebar.session.state, method);
				
			}
			
			// Set the current sidebar state
			this.state = method;
			
			// (:
			return this;
			
		},
		
		/**
		 * Sets the layout for an specific element
		 * @method setElementLayout
		 * @param {String|Object} element - Element to which the layout will be set
		 * @param {Object}        options - Options for the Layout object
		 * @return {CRD.Layout}
		 * @memberOf Sidebar
		 * @public
		 */
		
		setElementLayout : function(element, options) {
			
			// Sets the element
			element = jQuery(element);
			
			// If layout hasnt been initialized yet
			if(typeof element.layout('get') !== 'undefined') {
				
				// Sets the element options
				element.layout('setOptions', options);
				
			} else {
				
				// Initializes the layout element
				element.layout(options);
				
			}
			
			// (:
			return element.layout('get');
			
		},
		
		/**
		 * Toggles the sidebar
		 * @method toggleSidebar
		 * @return {Sidebar}
		 * @memberOf Sidebar
		 * @public
		 */
		
		toggleSidebar : function() {
			
			// Variables
			var method = this.state === constants.sidebar.Mode.CLOSE ?
				constants.sidebar.Mode.OPEN :
				constants.sidebar.Mode.CLOSE;
			
			// Toggles the layout
			this.setLayout(method);
			
			// (:
			return this;
			
		},
		
		/**
		 * Get the disabled sidebar status (thru the container - wrapper - element)
		 * @method disabled
		 * @return {boolean}
		 * @memberof Sidebar
		 * @public
		 */
		
		disabled : function() {
			
			// Check the container element for the layour status and returns it
			return this.wrapper.layout('get').disabled;
			
		}
		
	}, CRD.ClassUtils);
	
	// Set object constants
	CRD.Utils.setConstants(Sidebar, constants.sidebar);
	
	/**
	 * Layout
	 * Extends Sidebar, to enable to add elements in to the layout (sidebar or content container), thru method or
	 * by definend html data atributes.
	 * @class
	 * @param {Object} options - Options to override default layout options.
	 * @return {Sidebar}
	 */
	
	function Layout(options) {
		
		// If singleton has not been initialized
		if(Sidebar.prototype.instance === null) {
			
			// Set options and events
			this.setOptions(options);
			
			// Initializes the object
			this.Layout();
			
			// Apply base constructor with the supplied options
			Sidebar.apply(this);
			
		}
		
		// Returns the singleton instance
		return Sidebar.prototype.instance;
		
	}
	
	// Inherits from Sidebar
	Layout.inherit(Sidebar);
	
	// Adds new methods to the object prototype
	Layout.prototype = extend(Layout.prototype, (function() {
		
		// Variables
		var defaults = extend(true, {
				selector     : '[data-'+CRD.Layout.Storage.HTML+']',
				extraOptions : {
					'sidebar'   : {
						stack : true
					},
					'container' : {
						stack : true
					}
				}
			}, Sidebar.prototype.defaults);
		
		// Return the object prototype object
		return {
			
			/**
			 * Default options
			 * @property {Object} defaults              - Default options
			 * @property {String} defaults.selector     - Selector for the html data atributed elements
			 * @property {String} defaults.extraOptions - Extra element Layout options for each defined element
			 * @see Inherits default options from Layout.prototype.defaults
			 */
			
			defaults : defaults,
			
			/**
			 * Initializes Layout object
			 * @constructor
			 * @memberof Layout
			 * @public
			 */
			
			Layout : function() {
				
				// Get all html data atributed elements
				this.elements = jQuery(this.options.selector);
				
			},
			
			/**
			 * Adds an elenent to the layout
			 * @method addElement
			 * @param {Object|String} element - Element to add to the layout
			 * @return {Layout}
			 * @memberof Layout
			 * @public
			 */
			
			addElement : function(element) {
				
				// Add the element to the array of elements
				this.elements.push(element);
				
				// (:
				return this;
				
			},
			
			/**
			 * Sets the layout for all the defined elements, then triggers the Sidebar layout
			 * @method setLayout
			 * @param {String} mode - Sidebar mode (CRD.UI.Sidebar.Mode.OPEN or CRD.UI.Sidebar.Mode.CLOSE)
			 * @memberof Layout
			 * @override Sidebar.setLayout
			 * @public
			 */
			
			setLayout : function(mode) {
				
				// Variables
				var self = this,
					options;
				
				// For each extra element
				this.elements.each(function(i, element) {
					
					// Get the element options (html data atributed options)
					options = CRD.Layout.getOptions(element);
					
					// Set the layout for the element
					self.setElementLayout(element, options);
					
				});
				
				// Applies the original method
				Sidebar.prototype.setLayout.apply(this, [mode]);
				
			},
			
			/**
			 * Forces the resize of all the interface
			 * @method resize
			 * @return {Layout}
			 * @memberof Layout
			 * @public
			 */
			
			resize : function() {
				
				// Resizes the main container, that will trigger all resizes
				this.wrapper.layout('resize', true);
				
				// (:
				return this;
				
			}
			
		};
		
	})());
	
	/**
	 * Toolbar
	 * @class
	 * @param {String|Object} element - Element to use as toolbar container
	 * @param {Object}        options - Options to override default toolbar options.
	 * @return {Toolbar}
	 */
	
	function Toolbar(element, options) {
		
		// Uses the options as the last element specified
		options = Array.prototype.pop.call(arguments);
		
		// Set the variables so there is no need to specify and element if the class must create one
		element = arguments.length === 2 ?
			jQuery(element) :
			this.createContainer(options);
		
		// Variables
		var self = element.data(storage.toolbar); // Data storage reference
		
		// If instance hasn't been created yet
		if(typeof self === 'undefined') {
			
			// Set options and events
			this.setOptions(options);
			
			/**
			 * Array containing references to the toolbar buttons
			 * @property {Object} buttons - Array of buttons
			 */
			
			this.buttons = [];
			
			// Initializes the object
			this.Toolbar();
			
			// Stores the object reference in the element storage
			element.data(storage.toolbar, this);
			
		}
		
		// Return the toolbar
		return this;
		
	}
	
	// Toolbar prototype implements ClassUtils
	Toolbar.prototype = extend({
		
		/**
		 * Default options
		 * @property {Object} defaults             - Default options
		 * @property {String} defaults.constructor - Toolbar container constructor string (tag)
		 * @property {Object} defaults.layout      - Toolbar Layout instance options
		 * @property {Array}  defaults.buttons     - Initial toolbar buttons
		 */
		
		defaults : {
			constructor : '<div class="toolbar" />',
			layout      : {},
			buttons     : []
		},
		
		/**
		 * Initializes the Toolbar object
		 * @constructor
		 * @memberof Toolbar
		 * @public
		 */
		
		Toolbar : function() {
			
			// For each defined button
			for(var i = 0, max = this.options.buttons.length; i < max; i++) {
				
				// Adds the button
				this.addButton.call(this, this.options.buttons[i]);
				
			}
			
			// Hooks to the resize events
			this.element.on(CRD.Layout.Events.RESIZE, this.updateContainerWidth.bind(this));
			this.element.on(CRD.Layout.Events.DISABLE, this.updateContainerWidth.bind(this));
			
		},
		
		/**
		 * Creates the toolbar container. Sets the object options (only if the container hasnt been created) to be able
		 * to use them.
		 * @method createContainer
		 * @param {Object} options - Initial set of options
		 * @return {Object}
		 * @memberOf Toolbar
		 * @public
		 */
		
		createContainer : function(options) {
			
			// Variables
			var element, layout;
			
			// Set options and events
			this.setOptions(options);
			
			// If container hasnt been created
			if(typeof this.element === 'undefined') {
				
				// Creates the main element for the toolbar
				element = jQuery(this.options.constructor);
				
				// Creates the button holder
				element.append(jQuery('<div />'));
				
				// Initializes the layout for the element
				layout = new CRD.Layout(element, this.options.layout);
				
				// Set the element
				this.element = element;
				
			}
			
			// Return the element
			return this.element;
			
		},
		
		/**
		 * Updates the button container width, so everyone can fit in
		 * @method updateContainerWidth
		 * @memberOf Toolbar
		 * @public
		 */
		
		updateContainerWidth : function() {
			
			// Variables
			var buttons = this.element.find('div').children(), // Buttons
				total = 0, // Total width
				scrollable;
			
			// For each button
			buttons.each(function(i, e) {
				
				// Sets the element
				e = jQuery(e);
				
				// Measures the element (width + margin)
				total += Math.ceil(e.outerWidth(i < buttons.length - 1 ? true : false));
			
			});
			
			// If the container is larger than the available space
			if(total > this.element.innerWidth()) {
				
				// Sets the container width
				this.element.find('div').css('width', total);
				
				// Initializes the scrollable area
				scrollable = new CRD.Scrollable(this.element);
				
			} else {
				
				// Resets width
				this.element.find('div').css('width', 'auto');
				
			}
		
		},
		
		/**
		 * Adds (and creates) a button to the toolbar
		 * @param {Object}   options  - Options to override default button options.
		 * @param {String}   label    - Button label
		 * @param {Function} callback - Callback for the button
		 * @return {Toolbar}
		 */
		
		addButton: function(options, label, callback) {
			
			// Variables
			var args = Array.prototype.slice.call(arguments),
				button, constructor;
			
			// If supplied argument is an instance of a button
			if(args[0] instanceof Button === true) {
				
				// Assing the button as the current supplied instance of Button
				button = args[0];
				
			} else {
				
				// Creates the new instance intermetiate constructor
				constructor = function(args) {
					return new (Function.prototype.bind.apply(Button, [null].concat(args)));
				};
				
				// Initialized and stores the object
				button = constructor(args[0]);
				
			}
			
			// Append the button
			this.element.find('div').append(button.element);
			
			// Update the container width
			this.updateContainerWidth();
			
			// (:
			return this;
			
		}
		
	}, CRD.ClassUtils);
	
	/**
	 * Button
	 * @class
	 * @param {Object}   options - Options to override default button options.
	 * @param {String}   label   - Button label
	 * @param {Function} action  - Element to use as toolbar container
	 * @return {Button}
	 */
	
	function Button(options, label, action) {
		
		// Variables
		var args;
		
		// Set the arguments so there is no need to pass all the arguments
		// It can be [callback|url], [label, callback|url], [options, label, callback|url]
		args = Array.prototype.slice.call(arguments);
		action = Array.prototype.pop.call(args);
		label = Array.prototype.pop.call(args) || null;
		options = Array.prototype.pop.call(args) || {};
		
		// Set options and events
		this.setOptions(options);
		
		/**
		 * Button element
		 * @property {Object} element - Button element
		 */
		
		this.element = null;
		
		/**
		 * Action to execute
		 * @property {Function|String} action - Function or url
		 */
		
		this.action = action;
		
		/**
		 * Enabled state (allows or not a click event)
		 * @property {Object} element - Button enabled/disabled state
		 */
		
		this.enabled = true;
		
		/**
		 * First clicked position (to prevent click and drag)
		 * @property {Object} coordinates - Mousedown click coordinates
		 */
		
		this.coordinates = { x : null, y : null };
		
		// Initializes the object
		this.Button(label || this.options.label);
		
		// Stores the object reference in the element storage
		this.element.data(storage.button, this);
		
		// Returns the button
		return this;
		
	}
	
	// Toolbar prototype implements ClassUtils
	Button.prototype = extend({
		
		/**
		 * Default options
		 * @property {Object}         defaults             - Default options
		 * @property {String}         defaults.id          - Button default id
		 * @property {String}         defaults.action      - Button default action
		 * @property {String}         defaults.constructor - Button constructor string (tag)
		 * @property {String}         defaults.style       - Extra classes fot the button
		 * @property {String}         defaults.disabled    - Class for disbled buttons
		 * @property {String}         defaults.label       - Button label
		 * @property {Boolean|String} defaults.icon        - Button icon (using FontAwesome)
		 * @property {Boolean}        defaults.enabled     - Default enabled status
		 * @see http://fontawesome.io/cheatsheet
		 */
		
		defaults : {
			id          : null,
			action      : jQuery.noop,
			constructor : '<a class="button"><i class="fa"></i><span></span></a>',
			style       : 'default',
			disabled    : 'disabled',
			label       : 'Button',
			icon        : false,
			enabled     : true
		},
		
		/**
		 * Initializes the Button object
		 * @constructor
		 * @param {String} label - Button label
		 * @listen this.element.mousedown
		 * @listen this.element.mouseup
		 * @memberof Button
		 * @public
		 */
		
		Button : function(label) {
			
			// Variables
			var button, span, i;
			
			// Creates the element
			this.element = jQuery(this.options.constructor)
				.addClass(this.options.style);
			
			// If the button is not enabled
			if(this.options.enabled === false) {
				
				// Adds the disabled class
				this.element.addClass(this.options.disabled);
				
			}
			
			// If an id is defined
			if(this.options.id !== null) {
				
				// Sets the button id
				this.element.attr('id', this.options.id);
				
			}
			
			// Sets the element references
			button = this.element;
			span = button.find('span');
			i = button.find('i');
			
			// Sets the label and the icon
			span.text(label);
			
			// Sets or hide the button icon
			i = this.options.icon !== false ?
				i.addClass(this.options.icon) :
				i.css('display', 'none');
			
			// Sets the button event
			button.on({
				'mousedown' : this.mousedown.bind(this),
				'mouseup'   : this.mouseup.bind(this)
			});
			
			// Sets the default enabled status
			this.enabled = this.options.enabled;
			
		},
		
		/**
		 * Enabled the button
		 * @method enable
		 * @memberOf Button
		 * @public
		 */
		
		enable : function() {
			
			// If the button is disabled
			if(this.enabled === false) {
				
				// Enables the button
				this.enabled = true;
				
				// Reset the button classes
				this.element.removeClass(this.options.disabled);
				
			}
			
		},
		
		/**
		 * Disables the button
		 * @method disable
		 * @memberOf Button
		 * @public
		 */
		
		disable : function() {
			
			// If the button is disabled
			if(this.enabled === true) {
				
				// Enables the button
				this.enabled = false;
				
				// Reset the button classes
				this.element.addClass(this.options.disabled);
				
			}
			
		},
		
		/**
		 * Handles the mousedown event
		 * @method mousedown
		 * @param {Object} event - Event
		 * @memberOf Button
		 * @public
		 */
		
		mousedown : function(event) {
			
			// Stores the event coorinates
			this.coordinates = {
				x : event.pageX,
				y : event.pageY
			};
			
		},
		
		/**
		 * Handles the mouseup event
		 * @method mouseup
		 * @param {Object} event - Event
		 * @memberOf Button
		 * @public
		 */
		
		mouseup : function(event) {
			
			// If releasing the click in a near place
			if(event.pageX == this.coordinates.x && event.pageY == this.coordinates.y) {
				
				// Executes the action
				this.click();
				
			} else {
				
				// Resets the click
				this.coordinates = { x : null, y : null };
				
			}
			
			
		},
		
		/**
		 * Fires the button action, if enabled
		 * @method click
		 * @memberOf Button
		 * @public
		 */
		
		click : function() {
			
			// if button is enabled
			if(this.enabled === true && this.action !== null) {
				
				/**
				 * Triggers the click event
				 * @event crd.button.click
				 */
				
				this.dispatch(constants.button.Events.CLICK, [this.element]);
				
				// Switch betweeen different types of options
				switch(typeof this.action) {
					
					// Funtions
					case 'function':
						
						// Executes the function
						this.action.call(this.element);
						
						break;
					
					// Default (url)
					default:
						
						// Navigates to the url
						location.href = this.action;
						
						break;
					
				}
				
			}
			
		}
		
	}, CRD.ClassUtils);
	
	// Set object constants
	CRD.Utils.setConstants(Button, constants.button);
	
	/**
	 * Collapsable
	 * @class
	 * @param {String|Object} element - Main container for the togglers and content
	 * @param {Object}        options - Options to override default submenu options.
	 * @return {Submenu}
	 */
	
	function Collapsable(element, options) {
		
		// Element reference
		element = jQuery(element);
		
		// Variables
		var self = element.data(storage.collapsable.main); // Data storage reference
		
		// If instance hasn't been created yet
		if(typeof self === 'undefined') {
			
			// Set options and events
			this.setOptions(options);
			
			/**
			 * Element to use as container
			 * @property {Object} element - Element
			 */
			
			this.element = element;
			
			/**
			 * Toggle triggers
			 * @property {Object} triggers - Toggle triggers collection
			 */
			
			this.triggers = jQuery(this.options.triggers, this.element);
			
			/**
			 * Toggle containers
			 * @property {Object} containers - Toggle containers collection
			 */
			
			this.containers = jQuery(this.options.containers, this.element);
			
			// Initializes the object
			this.Collapsable();
			
			// Stores the object reference in the element storage
			this.element.data(storage.collapsable.main, this);
			
			// Sets data reference
			self = this;
			
		}
		
		// Returns object reference
		return self;
		
	}
	
	// Collapsable prototype implements ClassUtils
	Collapsable.prototype = extend({
		
		/**
		 * Default options
		 * @property {Object}   defaults            - Default options
		 * @property {String}   defaults.triggers   - Selector for the triggers
		 * @property {String}   defaults.containers - Selector for the containers
		 * @property {Function} defaults.show       - Function to show containers (inmediately)
		 * @property {Function} defaults.hide       - Function to hide containers (inmediately)
		 * @property {Function} defaults.toggle     - Function to toggle containers (inmediately)
		 */
		
		defaults : {
			triggers    : '.trigger',
			containers  : '.container',
			show        : function() { this.show(); },
			hide        : function() { this.hide(); },
			toggle      : function() { this.slideToggle(); }
		},
		
		/**
		 * Initializes the togglers
		 * @constructor
		 * @memberof Collapsable
		 * @public
		 */
		
		Collapsable : function() {
			
			// Variables
			var self = this;
			
			// For each defined container
			this.containers.each(function(i, container) {
				
				// Variables
				var state, mode;
				
				// Container
				container = jQuery(container);
				
				// Get the html data atribute
				state = container.data(storage.collapsable.html.state);
				
				// If the  state is defined
				if(typeof state !== 'undefined' && (state === constants.collapsable.Mode.OPEN || Number(state) === 0)) {
					
					// Shows the container
					self.options.show.apply(container);
					
					// Sets the state as opened
					mode = constants.collapsable.Mode.OPEN;
					
				} else {
					
					// Hides the container
					self.options.hide.apply(container);
					
					// Sets the state as closed
					mode = constants.collapsable.Mode.CLOSE;
					
				}
				
				// Sets the toggle state
				container.data(storage.collapsable.state, mode);
				
				/**
				 * Triggers the resize event
				 * @event crd.collapsable.open
				 * @event crd.collapsable.close
				 */
				
				self.dispatch(mode === constants.collapsable.Mode.OPEN ? constants.collapsable.Events.OPEN : constants.collapsable.Events.CLOSE, [
					jQuery(self.triggers[i]),
					container,
					self
				]);
				
			});
			
			// Creates the click handler for the triggers
			this.clickHandler = function(e) {
				e.preventDefault();
				this.toggle(e.currentTarget);
			}.bind(this);
			
			// Adds the triggers event
			this.triggers.click(this.clickHandler);
			
		},
		
		/**
		 * Toggles a specific content
		 * @method toggle
		 * @param {Object|Number} trigger - Trigger or index (numeric) for the content to toggle
		 * @fires CRD.Collapsable.Events.OPEN
		 * @fires CRD.Collapsable.Events.CLOSE
		 * @return {Collapsable}
		 * @memberOf Collapsable
		 * @public
		 */
		
		toggle : function(trigger) {
			
			// Element
			trigger = jQuery(typeof trigger === 'number' ? this.triggers[trigger] : trigger);
			
			// Variables
			var container = trigger.next(this.options.containers), // Container
				state = container.data(storage.collapsable.state), // Current state
				mode = constants.collapsable.Mode.OPEN,
				event = constants.collapsable.Events.OPEN;
			
			// If container is opened
			if(state === constants.collapsable.Mode.OPEN) {
				
				// Set the new mode and the event to trigger
				mode  = constants.collapsable.Mode.CLOSE;
				event = constants.collapsable.Events.CLOSE;
				
			}
			
			// Sets the new state
			container.data(storage.collapsable.state, mode);
			
			// Applies show/hide function to the menu
			this.options.toggle.apply(container, [mode]);
			
			/**
			 * Triggers the open or close event
			 * @event crd.collapsable.open
			 * @event crd.collapsable.close
			 */
			
			this.dispatch(event, [
				trigger,
				container,
				this
			]);
			
			// (:
			return this;
			
		},
		
		/**
		 * Closes a specific container determined by its index (or position in the array of containers)
		 * @method close
		 * @param {Number} index - Index of the container to close
		 * @fires CRD.Collapsable.Events.CLOSE
		 * @return {Collapsable}
		 * @memberOf Collapsable
		 * @public
		 */
		
		close : function(index) {
			
			// Variables
			var container = jQuery(this.containers[index]),
				trigger = jQuery(this.triggers[index]);
			
			// Sets the new state
			container.data(storage.collapsable.state, constants.collapsable.Mode.CLOSE);
			
			// closes the container
			this.options.hide.apply(container);
			
			/**
			 * Triggers the close event
			 * @event crd.submenu.close
			 */
			
			this.dispatch(constants.collapsable.Events.CLOSE, [
				trigger,
				container,
				this
			]);
			
			// (:
			return this;
			
		},
		
		/**
		 * Close all the submenus
		 * @method closeAll
		 * @param {Boolean} toggle - Use animated close (with the defined toggle function)
		 * @return {Collapsable}
		 * @memberOf Collapsable
		 * @public
		 */
		
		closeAll : function(toggle) {
			
			// Variables
			var self = this;
			
			// Animated?
			toggle = toggle || false;
			
			// For each submenu
			this.containers.each(function(i, container) {
				
				// If must use animation
				if(toggle === true && jQuery(container).data(storage.collapsable.state) === constants.collapsable.Mode.OPEN) {
					
					// Toggles to close the container
					self.toggle(i);
					
				} else {
					
					// Close the container
					self.close(i);
					
				}
				
			});
			
			// (:
			return this;
			
		}
		
	}, CRD.ClassUtils);
	
	// Set object constants
	CRD.Utils.setConstants(Collapsable, constants.collapsable);
	
	// Defines the jQuery helper
	CRD.Utils.defineHelper(Collapsable, 'collapsable', storage.collapsable.main);
	
	/**
	 * Submenu
	 * @class
	 * @param {String|Object} element - Menu element/container
	 * @param {Object}        layout  - Main Layout instance reference
	 * @return {Submenu}
	 */
	
	function Submenu(element, layout) {
		
		// Element reference
		element = jQuery(element);
		
		// Variables
		var self = element.data(storage.collapsable.main), // Data storage reference
			options;
		
		// If instance hasn't been created yet
		if(typeof self === 'undefined') {
			
			// Set options
			options = {
				
				// Custom selectors
				triggers   : '.submenu-trigger',
				containers : '.submenu-container',
				
				// Toggle function  (overrides default)
				toggle     : function(mode) {
					
					// Variables
					var state = layout.disabled() === true ?
						false : // Force toggle for mobile version
						layout.state;
					
					// Depending on the current state of the sidebar
					switch(state) {
						
						// Closed sidebar
						case CRD.UI.Sidebar.Mode.CLOSE:
							
							// Shows or hides the submenu
							this.css(mode === constants.collapsable.Mode.OPEN ? {
									'display'    : 'block',
									'visibility' : 'visible'
								} : {
									'display'    : 'none',
									'visibility' : 'hidden'
								});
							
							break;
						
						// Opened sidebar
						default:
							
							// Toggles the sidebar (and sets the visibility, to reset the floating state menu)
							this
								.css('visibility', 'visible')
								.slideToggle();
							
							break;
						
					}
					
				},
				
				// Events
				on         : {
					'crd.collapsable.open' : function(e, t, m, o) {
						
						// Adds the opened class
						t.parent().addClass('open');
						
					},
					'crd.collapsable.close' : function(e, t, m, o) {
						
						// removes the opened class
						t.parent().removeClass('open');
						
					}
				}
				
			};
			
			// Initializes the object
			self = Collapsable.apply(this, [element, options]);
			
			// For each submenu trigger
			this.triggers.each(function(i, trigger) {
				
				// Variables
				var index = i;
				
				// Sets the clickoutside handler
				jQuery(trigger).clickOutside(function() {
					
					// If not in mobile mode and sidebar is closed
					if(layout.disabled() === false && layout.state === constants.sidebar.Mode.CLOSE) {
						
						// Closes the submenu
						self.close(index);
						
					}
					
				}, jQuery(self.containers[i]));
				
			});
			
		}
		
		// Returns object reference
		return self;
		
	}
	
	// Inherits from Collapsable
	Submenu.inherit(Collapsable);
	
	// Returns the interface classes
	context = extend(true, context, {
		UI : {
			'Sidebar'     : Sidebar,
			'Layout'      : Layout,
			'Toolbar'     : Toolbar,
			'Button'      : Button,
			'Collapsable' : Collapsable,
			'Submenu'     : Submenu
		}
	});
	
})(CRD);