/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD');

// Creates the namepsace for the object
CRD.DataTable = (function() {
	"use strict";
	
	// Variables
	var storage = {
			main : 'crd.table'
		}; // Storage
	
	/**
	 * Table
	 * DataTable utility class and helpers
	 * @class
	 * @param {Object|String} element - Element to bind to DataTable instance
	 * @param {String}        url     - Ajax URL source.
	 * @param {Object}        options - Options to override default table options.
	 * @return {Table}
	 */
	
	function Table(element, url, options) {
		
		// Variables
		var self = jQuery(element).data(storage.main);
		
		// If the elements is not a DataTable instance
		if(jQuery.fn.dataTable.isDataTable(element) === false) {
			
			// Sets the element reference
			this.element = jQuery(element);
			
			// Set table options
			this.setTableOptions(url, options);
			
			// Initializes the DataTable instance
			this.instance = jQuery(element).DataTable(this.options.tableOptions);
			
			// Set row actions
			this.setRowsActions();
			
			// Sets the object instance
			self = this;
			
			// Stores the object instace
			this.element.data(storage.main, self);
			
		}
		
		// Return the table instance
		return self;
		
	}
	
	// Sidebar prototype implements ClassUtils
	Table.prototype = jQuery.extend({
		
		/**
		 * Default options
		 * @property {Object} defaults              - Default options
		 * @property {Object} defaults.tableOptions - DataTables Options @see https://www.datatables.net/reference/option/
		 *                                            for full list of DataTables options
		 */
		
		defaults : {
			tableKeys    : 'id',
			columns      : [],
			editable     : true,
			editUrl      : 'edit?id={id}',
			editConstr   : '<a class="edit" href="" title="Editar"><span>Editar</span></a>',
			deletable    : true,
			deleteUrl    : 'delete?id={id}',
			deleteConstr : '<a class="delete" href="" title="Eliminar"><span>Eliminar</span></a>',
			selectable   : true,
			tableOptions : {}
		},
		
		/**
		 * Default DataTable options
		 * @property {Object} defaultTableOptions - Default DataTable options
		 */
		
		defaultTableOptions : {
			stateSave  : false,
			scrollX    : false,
			processing : true,
			serverSide : true,
			responsive : true,
			language   : {
				url : '//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json'
			}
		},
		
		/**
		 * Set DataTable options including columns, and action buttons
		 * @param url
		 * @param options
		 */
		
		setTableOptions : function(url, options) {
			
			// Variables
			var tableOptions, columns = [], type,
				keys = options.hasOwnProperty('tableKeys') ? options.tableKeys : this.defaults.tableKeys,
				editable = (options.hasOwnProperty('editable') && options.editable === true) || this.defaults.editable === true,
				deletable = (options.hasOwnProperty('deletable') && options.deletable === true) || this.defaults.deletable === true;
			
			// If there is a definition for the columns
			if(options.hasOwnProperty('columns')) {
				
				// For each defined columns
				for(var x = 0, max = options.columns.length; x < max; x++) {
					
					// Gets the type for the definition
					type = typeof options.columns[x];
					
					// Depending on the data type
					switch(type) {
						
						// String, the data will be used as a simple data key for the provided data
						case 'string':
							columns.push({
								data : options.columns[x]
							});
							break;
							
						// Object, will use the data as a configuration data set for DataTables
						// @see https://datatables.net/reference/option/columns
						case 'object':
							columns.push(options.columns[x]);
							break;
						
					}
					
				}
				
				// If records are editable or deletable
				if(editable === true || deletable === true) {
					
					// Adds the edit/delete icons columns
					columns.push({
						data               : keys,
						searchable         : false,
						orderable          : false,
						className          : 'actions',
						render             : this.renderActions.bind(this),
						responsivePriority : 1
					});
					
				}
				
			}
			
			// Merge DataTable options
			tableOptions = jQuery.extend(true, {}, this.defaultTableOptions, {
				ajax    : url,
				columns : columns
			});
			
			// Merge all the options
			options = jQuery.extend(true, {}, { tableOptions : tableOptions }, options);
			
			// Set options
			this.setOptions(options);
			
		},
		
		/**
		 * Renders the actions cell for each row
		 * @param data
		 * @param type
		 * @param full
		 * @param meta
		 * @return {string}
		 * @see https://datatables.net/reference/option/columns.render
		 */
		
		renderActions : function(data, type, full, meta) {
			
			// Variables
			var html = jQuery('<div />'),
				edit, del;
			
			// If table is editable
			if(this.options.editable === true) {
				
				// Creates the edit icon
				edit = jQuery(this.options.editConstr)
					.attr('href', this.options.editUrl.substitute(full));
				
				// Appends the button to the container
				html.append(edit);
				
			}
			
			// If table is deletable
			if(this.options.deletable === true) {
				
				// Creates the delete icon
				del = jQuery(this.options.deleteConstr)
					.attr('href', this.options.deleteUrl.substitute(full));
				
				// Appends the button to the container
				html.append(del);
				
			}
			// (:
			return String(html.html());
			
		},
		
		/**
		 * Deletion confirmation
		 * @param {Object} event
		 */
		
		confirm : function(event) {
			
			// Variables
			var url = jQuery(event.target).attr('href'),
				modal;
			
			// Prevent default
			event.preventDefault();
			event.stopPropagation();
			
			// Creates de modal
			modal = new CRD.Modal.Confirm(function() {
				location.href = url;
			}, null, {
				message : '¿Desea eliminar el elemento seleccionado?'
			});
			
		},
		
		/**
		 * Set rows actions (selection) if enabled
		 */
		
		setRowsActions : function() {
			
			// If rows are selectable
			if(this.options.selectable === true) {
				
				// Sets the click event for the rows
				this.element.on('click', 'tr', function() {
					
					// Selects the row
					jQuery(this)
						.toggleClass('selected');
					
				});
				
			}
			
			// If elements are deletable
			if(this.options.deletable === true) {
				
				// Sets the click event for the deletion buttons
				this.element.on('click', 'a.delete', this.confirm.bind(this));
				
			}
			
		},
		
		/**
		 * Get selected rows
		 * @return {Object}
		 */
		
		getSelectedRows : function() {
			
			// Return selected rows
			return this.instance.rows('.selected').data();
			
		},
		
		/**
		 * Gets the DataTable instance
		 * @method table
		 * @return {DataTable}
		 * @memberof Table
		 * @public
		 */
		
		table : function() {
			
			// Returns the DataTable instance
			return this.instance;
			
		}
		
	}, CRD.ClassUtils);
	
	// Returns the Table class
	return Table;
	
})();