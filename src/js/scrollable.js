/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD');

// Creates the namepsace for the object
(function(context) {
	"use strict";
	
	// Variables
	var storage = {
			main  : 'crd.scrollable'
		}, // Data storage
		extend = jQuery.extend; // jQuery.extend shortcut
	
	/**
	 * Scrollable
	 * @class
	 * @param {Object} element - Scrollable element.
	 * @param {Object} options - Options to override default options.
	 * @return {Scrollable}
	 */
	
	function Scrollable(element, options) {
		
		// Element reference
		element = jQuery(element);
		
		// Variables
		var self = element.data(storage.main); // Data storage reference
		
		// If instance hasn't been created yet
		if(typeof self === 'undefined') {
			
			// Set options and events
			this.setOptions(options);
			
			/**
			 * Element to use as scrollable element
			 * @property {Object} element - Element
			 */
			
			this.element = element;
			
			/**
			 * Dragging flag
			 * @property {Boolean} dragging - Drag status
			 */
			
			this.dragging = false;
			
			/**
			 * Strart dragging points
			 * @property {Object} startPoint - X and Y start dragging coordinates
			 */
			
			this.startPoint = { x : 0, y : 0 };
			
			// Initializes the object
			this.Scrollable();
			
			// Stores the object reference in the element storage
			this.element.data(storage.main, this);
			
			// Sets data reference
			self = this;
			
		}
		
		// Returns object reference
		return self;
		
	}
	
	// Function prototype
	Scrollable.prototype = extend(true, {
		
		defaults : {
		
		},
		
		/**
		 * Initializes the scrollable area
		 * @constructor
		 * @memberOf Scrollable
		 * @listen this.element.mousemove
		 * @listen this.element.mousedown
		 * @listen this.element.mouseup
		 * @public
		 */
		
		Scrollable : function() {
			
			// Set document events
			jQuery(document.body).on({
				'mousedown'  : this.dragStart.bind(this),
				'touchstart' : this.dragStart.bind(this),
				'mouseup'    : this.dragStop.bind(this),
				'touchend'   : this.dragStop.bind(this),
				'mousemove'  : this.drag.bind(this),
				'touchmove'  : this.drag.bind(this)
			});
		
		},
		
		/**
		 * Starts the dragging
		 * @method dragStart
		 * @param {Object} event - Document event
		 * @memberOf Scrollable
		 * @public
		 */
		
		dragStart : function(event) {
			
			// Variables
			var touch = event.type.toLowerCase() == 'mousedown' ? false : true,
				target = jQuery(touch === false ? event.target : event.originalEvent.touches[0].target);
			
			// If mousedown on the element
			if(target.is(this.element) || target.closest(this.element).length > 0) {
				
				// Set the starting point
				this.startPoint = {
					x : touch === false ? event.pageX : event.originalEvent.touches[0].pageX,
					y : touch === false ? event.pageY : event.originalEvent.touches[0].pageY
				};
				
				// Sets the dragging status
				this.dragging = true;
				
			}
			
		},
		
		/**
		 * Stops the dragging
		 * @method dragStop
		 * @memberOf Scrollable
		 * @public
		 */
		
		dragStop : function() {
			
			// Sets the dragging status
			this.dragging = false;
			
		},
		
		/**
		 * performs the scrolling (by dragging)
		 * @method drag
		 * @param {Object} event - Document event
		 * @memberOf Scrollable
		 * @public
		 */
		
		drag : function(event) {
			
			// Variables
			var touch = event.type.toLowerCase() == 'mousemove' ? false : true;
		
			// If dragging?
			if(this.dragging === true) {
				
				// Sets the element scroll according to the mouse movement
				this.element.prop('scrollLeft', this.element.prop('scrollLeft') + (this.startPoint.x - (touch === false ? event.pageX : event.originalEvent.touches[0].pageX)));
				
			}
		
		}
		
	}, CRD.ClassUtils);
	
	// Returns the class
	context = extend(true, context, {
		'Scrollable' : Scrollable
	});
	
})(CRD);