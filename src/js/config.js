/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD.UI');

// Clobal configuration options
CRD.UI.Config = {
	
	// Login
	Login      : {
		formWidth     : 280 // Login form/box width
	},
	
	// Error pages
	ErrorPages : {
		messageWidth  : 420, // Error box width
		messageHeigth : 280  // Error box height
	},
	
	// Sidebar
	Sidebar    : {
		openWidth     : 250, // Opened sidebar width
		closedWidth   : 50,  // Closed sidebar width
		disableBefore : 767  // Disable sidebar/layout before
	}
	
};