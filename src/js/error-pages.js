/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD.UI');

// Creates the namepsace for the object
(function(context) {
	"use strict";
	
	// Variables
	var config = context.Config, // Configuration
		extend = jQuery.extend; // jQuery.extend shortcut
	
	/**
	 * ErrorPage
	 * @class
	 * @return {ErrorPage}
	 */
	
	function ErrorPage() {
		
		// Set layout
		this.ErrorPage();
		
	}
	
	// Function prototype
	ErrorPage.prototype = {
		
		/**
		 * Set layout elements
		 * @constructor
		 * @memberOf ErrorPage
		 * @public
		 */
		
		ErrorPage : function() {
			
			// Variables
			var options = {
				disabledBefore : 0
			}; // Common options
			
			// Main wrapper container
			jQuery('#wrapper').layout(options);
			
			// Login box
			jQuery('#container')
				.layout(extend(true, {
					width  : config.ErrorPages.messageWidth,
					height : config.ErrorPages.messageHeigth,
					top    : null,
					right  : null,
					bottom : null,
					left   : null,
					stack  : true
				}, options));
			
			// Footer copy
			jQuery('#copy')
				.layout(extend(true, {
					height : 20,
					top    : null,
					right  : 0,
					bottom : 0,
					left   : 0
				}, options));
			
		}
		
	};
	
	// Returns the interface classes
	context = extend(true, context, {
		'ErrorPage' : ErrorPage
	});
	
})(CRD.UI);