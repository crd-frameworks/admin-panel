/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD');

// Creates the namepsace for the object
CRD.ImageGrid = (function() {
	"use strict";
	
	// Variables
	var storage = {
			grid : {
				main : 'crd.imagegrid'
			},
			item : {
				main : 'crd.imagegrid.item'
			}
		}, // Storage
		constants = {
			request : {
				Events : {
					SUCCESS : 'crd.request.success',
					ERROR   : 'crd.request.error'
				}
			},
			item    : {
				Events : {
					RENDER : 'crd.imagegrid.item.render'
				}
			}
		},
		extend = jQuery.extend; // Extend shortcut
	
	/**
	 * ImageGrid
	 * @class
	 * @param {Object|String} element - Grid container
	 * @param {Object}        options - Options to override default grid options.
	 * @return {ImageGrid}
	 */
	
	function ImageGrid(element, options) {
		
		// Variables
		var self = jQuery(element).data(storage.grid.main);
		
		// If the elements is not a Grid instance
		if(typeof self === 'undefined') {
			
			// Set object options
			this.setOptions(options);
			
			/**
			 * Grid element container
			 * @property {Object} element - Element
			 */
			
			this.element = jQuery(element);
			
			/**
			 * Data related properties
			 * @property {Object} data - Loaded data
			 * @property {Number} page - Current page
			 */
			
			this.data = {};
			this.page = 0;
			
			/**
			 * Processing data or dom?
			 * @property {Boolean} processing - State of processing data or dom elements
			 */
			
			this.processing = true;
			
			// Initializes the grid
			this.ImageGrid();
			
			// Sets the object instance
			self = this;
			
			// Stores the object instace
			this.element.data(storage.grid.main, self);
			
		}
		
		// Return the table instance
		return self;
		
	}
	
	// ImageGrid prototype implements ClassUtils
	ImageGrid.prototype = jQuery.extend({
		
		/**
		 * Default options
		 * @property {Object} defaults - Default options
		 */
		
		defaults : {
			selectable   : true,
			deletable    : true,
			editable     : true,
			url          : null,
			pageLength   : 10,
			itemDefaults : {
				assetsUrl : '../assets',
				editUrl   : 'edit?id={id}',
				deleteUrl : 'delete?id={id}',
			},
			constructors : {
				container : '<div class="row grid"><div class="col-xs-60"></div></div>',
				toolbar   : '<div class="row grid-tools">' +
								'<div class="col-xs-60 col-sm-30 grid-length">' +
									'<label for="grid-length">{length}</label>' +
									'<select name="grid-length" id="grid-length">' +
										'<option value="10">10</option>' +
										'<option value="25">25</option>' +
										'<option value="50">50</option>' +
										'<option value="100">100</option>' +
									'</select>' +
								'</div>' +
								'<div class="col-xs-60 col-sm-30 grid-search">' +
									'<label for="grid-search">{search}</label>' +
									'<input type="search" name="grid-search" id="grid-search">' +
								'</div>' +
							'</div>',
				list      : '<div class="row grid-list"></div>',
				info      : '<div class="row grid-info">' +
								'<div class="col-xs-60 col-sm-30 grid-size"></div>' +
								'<div class="col-xs-60 col-sm-30 grid-paging">' +
									'<a href="javascript:;" class="grid-page-button previous disabled">{previous}</a>' +
									'<span></span>' +
									'<a href="javascript:;" class="grid-page-button next disabled">{next}</a>' +
								'</div>' +
							'</div>',
				page      : '<a href="javascript:;" data-grid-page="" class="grid-page-button">{label}</a>',
				loading   : '<div class="grid-processing"><div class="message">{loading}</div></div>',
				item      : Item
			},
			language : {
				length   : 'Mostrar',
				search   : 'Buscar:',
				paging   : {
					previous : 'Anterior',
					next     : 'Siguiente'
				},
				info     : 'Mostrando registros del {from} al {to} de un total de {total} registros',
				filtered : '(filtrado de un total de {total} registros)',
				empty    : 'Ningún dato disponible en esta tabla',
				edit     : 'Editar',
				delete   : 'Eliminar',
				loading  : 'Procesando...',
				confirm  : '¿Desea eliminar el elemento seleccionado?'
			}
		},
		
		/**
		 * Initializes the grid
		 * @memberOf Grid
		 * @constructor
		 * @public
		 */
		
		ImageGrid : function() {
			
			// Render the basic UI
			this.construct();
			
			// Set UI events
			this.setGridActions();
			
			// Inserts the grid
			this.element.append(this.container);
			
			// Loads data
			this.loadData();
			
		},
		
		/**
		 * Fully constructs the grid basic UI
		 * @method construct
		 * @memberOf Grid
		 * @public
		 */
		
		construct : function() {
			
			// Creates the table container
			this.container = jQuery(this.options.constructors.container);
			
			// Creates the tools container
			this.toolbar = jQuery(this.options.constructors.toolbar.substitute(this.options.language));
			
			// Creates the references to the toolbar controls
			this.gridLength = this.toolbar.find('.grid-length select').val(this.options.pageLength);
			this.gridSearch = this.toolbar.find('.grid-search [type="search"]');
			
			// Creates the list container
			this.list = jQuery(this.options.constructors.list);
			
			// Creates the grid info
			this.info = jQuery(this.options.constructors.info.substitute(this.options.language.paging));
			
			// Creates the references to the info containers
			this.gridInfo = this.info.find('.grid-size');
			this.gridPaging = this.info.find('.grid-paging');
			
			// Creates the tools container
			this.loading = jQuery(this.options.constructors.loading.substitute(this.options.language));
			
			// Nests elements
			this.container.find(':first-child').append(this.toolbar, this.list, this.info);
			this.container.append(this.loading);
			
		},
		
		/**
		 * Set events for page length, search and paging
		 * @method setEvents
		 * @memberOf ImageGrid
		 * @public
		 */
		
		setGridActions : function() {
			
			// Sets the grid length event
			this.gridLength.change(this.loadData.bind(this));
			
			// Sets the search event
			this.gridSearch.keyup(this.loadData.bind(this).debounce(500));
			
			// Sets the paging events
			this.gridPaging.on('click', 'a:not(.disabled)', function(e) {
				
				// If there is no running process
				if(this.processing === false) {
					
					// Variables
					var element = jQuery(e.target),
						page;
					
					// If page link is not disabled
					if(this.element.hasClass('current') === false) {
						
						// Sets the page
						page = element.data('grid-page');
						page = typeof page === 'undefined' ? (this.page + (element.hasClass('previous') ? -1 : 1)) : page;
						
						// Loads the selected page
						this.loadData(page);
						
					}
					
				}
				
			}.bind(this));
			
			// Items delegated events
			this.list.on('click', '.item', function(e) {
				
				// Variables
				var element = jQuery(e.target),
					url, modal;
				
				// Switch between different targets
				switch(e.target.tagName.toLowerCase()) {
					
					// Links
					case 'a':
						
						// If the clicked link is the delete one
						if(element.hasClass('delete')) {
							
							// Sets the deletion url
							url = element.attr('href');
							
							// Prevent default
							e.preventDefault();
							e.stopPropagation();
							
							// Creates de modal
							modal = new CRD.Modal.Confirm(function() {
								location.href = url;
							}, null, {
								message : this.options.language.confirm
							});
							
						}
						
						break;
					
					// Anything else
					default:
						
						// If items are selectable
						if(this.options.selectable === true) {
							
							// Selects the item
							jQuery(e.currentTarget)
								.toggleClass('selected');
							
						}
						
						break;
					
				}
				
			}.bind(this));
			
		},
		
		/**
		 * Loads data from the server
		 * @method loadData
		 * @param {Number} page   - Page to load
		 * @param {Object} params - Extra resquest parameters (to allow plugin to pass extra params)
		 * @memberOf ImageGrid
		 * @public
		 */
		
		loadData : function(page, params) {
			
			// Sets the page
			this.page = typeof page === 'number' ? page : 1;
			
			// If the request is not created
			if(typeof this.request === 'undefined') {
				
				// Creates the request
				this.request = new Request(this.options.url);
				
				// Hooks events
				jQuery(this.request).on(constants.request.Events.SUCCESS, this.render.bind(this));
				
			}
			
			// Set the processing status
			this.processing = true;
			
			// Shows the loading/processing message
			this.loading.show();
			
			// Loads the requested page
			this.request.load(extend({
				start  : this.from(), // Starting record (current page * records per page)
				length : this.length(), // Records per page
				search : this.gridSearch.val() // Search string
			}, params || {}));
			
		},
		
		/**
		 * Gets the elements per mage
		 * @method length
		 * @memberOf ImageGrid
		 * @return {number}
		 * @public
		 */
		
		length : function() {
			
			// Gets the select value
			return Number(this.gridLength.val());
			
		},
		
		/**
		 * Gets the starting record position
		 * @method from
		 * @memberOf ImageGrid
		 * @return {number}
		 * @public
		 */
		
		from : function() {
			
			// Gets the computed valur for the starting record
			return (this.page - 1) * this.length();
			
		},
		
		/**
		 * Gets the elements per mage
		 * @method to
		 * @memberOf ImageGrid
		 * @return {number}
		 * @public
		 */
		
		to : function() {
			
			// Gets the max value for the current page
			return Math.min(this.page * this.length(), this.data.recordsFiltered);
			
		},
		
		/**
		 * Queries the items for specific selection, and returns an array of items
		 * @method items
		 * @param {String|Object} condition - Selector to match
		 * @memberOf ImageGrid
		 * @return {Array}
		 * @public
		 */
		
		items : function(condition) {
			
			// Variales
			var items = this.list.find('.item'),
				type = typeof condition,
				matched = [], instance, i, add;
			
			// For each matched element
			items.each(function(i, e) {
				
				// Element
				e = jQuery(e);
				
				// Instance
				instance = e.data(storage.item.main);
				
				// Switches between types
				switch(type) {
					
					// if the condition is a string (then it will be treated as a selector)
					case 'string':
					
						// Adds the item to the list if it fits the condition
						if(e.is(condition)) {
							
							// Push the Item instance in to the items array
							matched.push(instance);
							
						}
						
						break;
					
					// If the condition is an object
					case 'object':
						
						// Assumes that the instance will be added as a matched element
						add = true;
						
						// For each condition property
						for(i in condition) {
							
							// Filters unwanted properties
							if(condition.hasOwnProperty(i) === true) {
								
								// If the instance does not have the condition key or the value is not the same value and type as in the condition
								if(instance.data.hasOwnProperty(i) === false || instance.data[i] !== condition[i]) {
									
									// Instance will not be addded
									add = false;
									
								}
								
							}
							
						}
						
						// If the instace matches all conditions
						if(add === true) {
							
							// Push the Item instance in to the items array
							matched.push(instance);
							
						}
						
						break;
					
				}
				
			}.bind(this));
			
			// (:
			return matched;
			
		},
		
		/**
		 * Renders the loaded data
		 * @method render
		 * @param {Object} event - Request event
		 * @param {Number} data  - Loaded data
		 * @memberOf ImageGrid
		 * @public
		 */
		
		render : function(event, data) {
			
			// Variables
			var defaults = jQuery.extend({
					deletable : this.options.deletable,
					editable  : this.options.editable
				}, this.options.itemDefaults),
				x, max, info, item, items = [];
			
			// Sets the data
			this.data = data;
			
			// If there are any records
			if(data.data.length > 0) {
			
				// For each record
				for(x = 0, max = data.data.length; x < max; x++) {
					
					// Creates the item
					item = new this.options.constructors.item(this, data.data[x], defaults);
					
					// Push the item to the items array
					items.push(item);
					
				}
				
				// Render all the items
				this.list
					.empty()
					.removeClass('empty')
					.append(items);
			
			} else {
				
				// Empties the grid
				this.list
					.empty()
					.addClass('empty')
					.text(this.options.language.empty);
				
			}
			
			// Sets the info for the table
			info = this.data.data.length > 0 ? {
				from  : Math.max(1, this.from()),
				to    : this.to(),
				total : data.recordsFiltered
			} : {
				from  : 0,
				to    : 0,
				total : 0
			};
			
			// Updates the grid info
			this.gridInfo.text(this.options.language.info.substitute(info) + (data.recordsFiltered < data.recordsTotal ? ' ' + this.options.language.filtered.substitute({ total : data.recordsTotal }) : ''));
			
			// Updates the pagination
			this.updatePaging();
			
			// Processing stopped
			this.processing = false;
			
			// Hides the loading/processing message
			this.loading.hide();
			
		},
		
		/**
		 * Updates the paging control
		 * @method updatePaging
		 * @memberOf ImageGrid
		 * @public
		 */
		
		updatePaging : function() {
			
			// Variables
			var container = this.info.find('.grid-paging span'),
				x, max = Math.ceil(this.data.recordsTotal / this.length()),
				page, pages = [];
			
			// For each page
			for(x = 1; x <= max; x++) {
				
				// Creates the page
				page = jQuery(this.options.constructors.page.substitute({ label : x }));
				
				// Sets the page data
				page.data('grid-page', x);
				
				// if this is the current page
				if(x === this.page) {
					
					// Sets the page as selected
					page.addClass('current');
					
				}
				
				// Push the page to the pages array
				pages.push(page);
				
			}
			
			// Enables/disables the next page button
			this.gridPaging.find('.next')[this.page < max ? 'removeClass' : 'addClass']('disabled');
			
			// Enables the previous page button
			this.gridPaging.find('.previous')[this.page > 1 ? 'removeClass' : 'addClass']('disabled');
			
			// Updates the container
			container.empty().append(pages);
		
		}
		
	}, CRD.ClassUtils);
	
	/**
	 * Request
	 * @class
	 * @param {String} url     - Request URL
	 * @param {Object} options - Options to override default request options.
	 * @return {Request}
	 */
	
	function Request(url, options) {
		
		/**
		 * URL to load
		 * @property {String} url - Request URL
		 */
		
		this.url = url;
		
		// Set options and events
		this.setOptions(options);
		
	}
	
	// Request prototype implements ClassUtils
	Request.prototype = jQuery.extend({
		
		/**
		 * Default options
		 * @property {Object} defaults           - Default options
		 * @property {string} defaults.method    - Default request method
		 * @property {Object} defaults.dataType  - Expected response data type
		 * @property {Object} defaults.extraData - Additional data to send with the request
		 */
		
		defaults : {
			method    : 'get',
			dataType  : 'json',
			extraData : {}
		},
		
		/**
		 * Executes the request
		 * @method load
		 * @param {Object} data - Data to send as request params
		 * @fires crd.request.success
		 * @fires crd.request.error
		 * @memberof Request
		 */
		
		load : function(data) {
			
			// Normalizes data
			data = data || {};
			
			// Variables
			var extra = this.options.extraData.length > 0 ?
					'&' + jQuery.param(extend({}, this.options.extraData)) :
					'',
				request;
			
			// Send ajax request
			request = new jQuery.ajax({
				url      : this.url,
				method   : this.options.method,
				dataType : this.options.dataType,
				cache    : false,
				data     : jQuery.param(data) + extra,
				error    : function(xhr, status, error) {
					
					/**
					 * Triggers events
					 * @event CRD.Request.Events.ERROR
					 */
					
					this.dispatch(constants.request.Events.ERROR, [
						xhr,
						status,
						error
					]);
					
				}.bind(this),
				success  : function(data, status, xhr) {
					
					/**
					 * Triggers events
					 * @event CRD.Request.Events.SUCCESS
					 */
					
					this.dispatch(constants.request.Events.SUCCESS, [
						data,
						status,
						xhr
					]);
					
				}.bind(this)
			});
			
			// (:
			return request;
			
		}
		
	}, CRD.ClassUtils);
	
	// Set object constants
	CRD.Utils.setConstants(Request, constants.request);
	
	// Adds the request to the ImageGrid namespace
	ImageGrid.Request = Request;
	
	/**
	 * Item
	 * @class
	 * @param {ImageGrid} grid    - Reference to the grid
	 * @param {Object}    data    - Item data
	 * @param {Object}    options - Options to override default item options.
	 * @return {Item}
	 */
	
	function Item(grid, data, options) {
		
		// Set object options
		this.setOptions(options);
		
		/**
		 * Grid reference
		 * @property {ImageGrid} grid - Reference to the grid itself
		 */
		
		this.grid = grid;
		
		/**
		 * Item data
		 * @property {Object} data - Item data
		 */
		
		this.data = data;
		
		/**
		 * Grid element container
		 * @property {Object} element - Element
		 */
		
		this.element = null;
		
		// Initializes the grid
		this.Item();
		
		// Stores the object instace
		this.element.data(storage.item.main, this);
		
		// Returns the reference to the element
		return this.element;
		
	}
	
	// Item prototype implements ClassUtils
	Item.prototype = extend({
		
		/**
		 * Default options
		 * @property {Object} defaults - Default options
		 */
		
		defaults : {
			assetsUrl    : '../assets',
			editable     : true,
			editUrl      : 'edit?id={id}',
			deletable    : true,
			deleteUrl    : 'delete?id={id}',
			constructors : {
				item   : '<div class="col-xs-60 col-sm-20 col-md-15 col-lg-12 item">' +
							'<div class="row">' +
								'<div class="col-xs-60 image">' +
									'<img src="{assetsUrl}/placeholder.png" alt="" class="placeholder">' +
									'<div class="loading">' +
										'<div class="animation-container">' +
											'<div></div> <div></div> <div></div>' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<div class="col-xs-60 data">' +
									'<div class="row">' +
										'<div class="col-xs-40">'+
											'<h2>{title}</h2>' +
											'<p>{description}</p>' +
										'</div>' +
										'<div class="col-xs-20 actions"></div>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>',
				edit   : '<a class="edit" href="{editUrl}" title="{edit}"><span>{edit}</span></a>',
				delete : '<a class="delete" href="{deleteUrl}" title="{delete}"><span>{delete}</span></a>'
			}
		},
		
		/**
		 * initializes the grid item
		 * @constructor
		 * @memberOf Item
		 * @fires crd.imagegrid.item.render
		 * @public
		 */
		
		Item : function() {
			
			// Creates the item
			this.element = jQuery(this.options.constructors.item.substitute(extend({
				assetsUrl : this.options.assetsUrl
			}, this.data, this.grid.options.language)));
			
			// Redner actions
			this.renderActions();
			
			// Fires the render event
			this.grid.dispatch(constants.item.Events.RENDER, [this.element, this.data, this]);
			
			// Loads the image
			this.loadImage();
		
		},
		
		/**
		 * Renders the buttons
		 * @method renderActions
		 * @memberOf Item
		 * @public
		 */
		
		renderActions : function() {
			
			// Variable
			var buttons = [];
			
			// If item can be edited
			if((this.data.hasOwnProperty('editable') === true && (this.data.editable === true || Number(this.data.editable) === 1)) || (this.data.hasOwnProperty('editable') === false && this.options.editable === true)) {
				
				// Creates the edit button
				this.edit = jQuery(this.options.constructors.edit.substitute(extend({
					editUrl   : this.options.editUrl.substitute(this.data)
				}, this.data, this.grid.options.language)));
				
				// Push the button in to the buttons array
				buttons.push(this.edit);
				
			}
			
			// If item can be deleted
			if((this.data.hasOwnProperty('deletable') === true && (this.data.deletable === true || Number(this.data.deletable) === 1)) || (this.data.hasOwnProperty('deletable') === false && this.options.deletable === true)) {
				
				// Creates the delete button
				this.delete = jQuery(this.options.constructors.delete.substitute(extend({
					deleteUrl : this.options.deleteUrl.substitute(this.data)
				}, this.data, this.grid.options.language)));
				
				// Push the button in to the buttons array
				buttons.push(this.delete);
				
			}
			
			// If there are any buttons to append
			if(buttons.length > 0) {
				
				// Appends the buttons
				this.element.find('.actions').append(buttons);
				
			}
			
		},
		
		/**
		 * Loads the image
		 * @method loadImage
		 * @memberOf Item
		 * @public
		 */
		
		loadImage : function() {
			
			// Creates the image to load
			this.image = jQuery('<img />');
			
			// Hooks the load event
			this.image.load(this.displayImage.bind(this));
			
			// Sets the image source
			this.image.attr('src', this.data.src);
			
		},
		
		/**
		 * Displays the image
		 * @method displayImage
		 * @memberOf Item
		 * @public
		 */
		
		displayImage : function() {
			
			// Inserts the image
			this.element.find('.image').append(this.image);
			
			// Fades out the loading animation
			this.element.find('.loading').fadeOut(400, function() {
				
				// Fades in the image
				this.image.fadeIn();
				
			}.bind(this));
			
		}
	
	}, CRD.ClassUtils);
	
	// Sets the constants
	CRD.Utils.setConstants(Item, constants.item);
	
	// Adds the item to the ImageGrid namespace
	ImageGrid.Item = Item;
	
	// Returns the Grid class
	return ImageGrid;
	
})();