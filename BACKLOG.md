## To-Do's

- Documentación
- Separar estilos de customización y estilos de armado
- Implementar eventos puros de JS
- Separar archivos JS muy largos en partes (ver como implementarlo)
- Ver to-do's
- Implementar configuraciones (inline y archivo) para JSHint
- Agregar dropdown a toolbar
- Agregar opciones ocultas a toolbar (si hay overflow)
- Forzar new en las classes (enforce object creation)

### CRD.Layout

- Tomar las opciones desde el HTML
- Opciones de resize para responsive

### CRD.Table

- Que la funcion haga el html donde se va a alojar la tabla
- Que las acciones (edit, delete) puedan tener eventos de javascript