module.exports = function(grunt) {
	
	grunt.initConfig({
		pkg    : grunt.file.readJSON('package.json'),
		concat : {
			js  : {
				options : {
					stripBanners  : true,
					sourceMap     : true,
					sourceMapName : 'lib/jsMap',
				},
				src     : [
					'lib/utils/globals.js',
					'lib/utils/function.js',
					'lib/utils/string.js',
					'lib/utils/utils.js',
					'lib/utils/class-utils.js',
					'lib/utils/session.js',
					'lib/layout/layout.js',
					'lib/layout/stack.js',
					'lib/breakpoint-spy/breakpoint-spy.js',
					'lib/breakpoint-spy/bootstrap-3-css-rules.js',
					'lib/interchange/interchange.js',
					'lib/interchange/data-attributed-bootstrap-3.js',
					'lib/ajax-forms/ajax-forms.js',
					'lib/form-post/form-post.js',
					'lib/jquery-validation/dist/jquery.validate.js',
					'lib/jquery-validation/dist/additional-methods.js',
					'lib/jquery-validation/dist/localization/messages_es_AR.js',
					'src/js/config.js',
					'src/js/ui.js',
					'src/js/scrollable.js',
					'src/js/tables.js',
					'src/js/grids.js',
					'src/js/modal.js',
					'src/js/login.js',
				],
				dest    : 'lib/scripts.js',
			},
			css : {
				src  : [
					'lib/bootstrap/css/bootstrap.min.css',
					'src/less/styles.css',
				],
				dest : 'dist/css/styles.css',
			},
		},
		cssmin : {
			target : {
				files : {
					'dist/css/styles.css' : [
						'lib/bootstrap/css/bootstrap.min.css',
						'src/less/styles.css',
					]
				}
			}
		},
		copy   : {
			js     : {
				files : [
					{
						src  : 'lib/scripts.js',
						dest : 'dist/js/main.js',
					},
				],
			},
			assets : {
				files : [
					{
						expand  : true,
						cwd     : 'assets/',
						src     : '**',
						dest    : 'dist/assets/',
						filter  : 'isFile',
					},
					{
						expand  : true,
						cwd     : 'lib/Font-Awesome/fonts/',
						src     : '**',
						dest    : 'dist/assets/fonts/',
						filter  : 'isFile',
					},
				]
			},
		},
		uglify : {
			production : {
				options : {
					sourceMap               : false,
					sourceMapIncludeSources : true,
					sourceMapIn             : 'lib/jsMap', // input sourcemap from a previous compilation
				},
				files   : {
					'dist/js/main.js' : [
						'lib/scripts.js',
					],
				},
			},
		},
		less   : {
			development : {
				options : {
					paths   : [
						'lib/mixins',
					],
					plugins : [
						new (require('less-plugin-autoprefix'))({browsers : ["last 2 versions"]}),
					],
				},
				files   : [
					{
						expand : true,
						cwd    : 'src/less/',
						src    : [
							'*.less',
							'!_*.less',
						],
						dest   : 'src/less/',
						ext    : '.css',
					},
				],
			},
			production  : {
				options : {
					paths       : [
						'lib/mixins',
					],
					compress    : true,
					yuicompress : true,
					plugins     : [
						new (require('less-plugin-autoprefix'))({browsers : ["last 2 versions"]}),
						require('less-plugin-inline-urls'),
						require('less-plugin-group-css-media-queries'),
						new (require('less-plugin-clean-css'))(),
					],
					modifyVars  : {
						'fa-font-path' : '"../assets/fonts"'
					},
				},
				files   : [
					{
						expand : true,
						cwd    : 'src/less/',
						src    : [
							'*.less',
							'!_*.less',
						],
						dest   : 'src/less/',
						ext    : '.min.css',
					},
				],
			},
		},
		watch  : {
			less : {
				files   : [
					'src/less/*.less',
				],
				tasks   : [
					'compile-dev',
				],
				options : {
					spawn     : false,
					interrupt : true,
				},
			},
			js   : {
				files   : [
					'src/js/*.js',
				],
				tasks   : [
					'concat-and-copy-js',
				],
				options : {
					spawn     : false,
					interrupt : true,
				},
			},
		},
		clean  : {
			build : [
				'dist/css/*.css',
				'src/less/*.css',
				'!css/styles.css',
				'!css/ie-10.css',
				'lib/jsMap',
				'lib/scripts.js',
			],
		},
	});
	
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	
	grunt.registerTask('build-dev', [
		'concat:js',
		'copy:js',
		'less:development',
		'concat:css'
	]);
	
	grunt.registerTask('build-prod', [
		'concat:js',
		'uglify:production',
		'less:production',
		'cssmin',
		'copy:assets',
		'clean:build'
	]);
	
	grunt.registerTask('compile-dev', [
		'less:development',
		'concat:css'
	]);
	
	grunt.registerTask('concat-and-copy-js', [
		'concat:js',
		'copy:js'
	]);
	
	grunt.registerTask("watch-js", ['watch:js']);
	grunt.registerTask("watch-less", ['watch:less']);
	
};